# PREAMBULE

Ce repo ne contient qu'un extrait des fichiers nécessaires au fonctionnement de l'application.  
Une simple copie de ce repo ne permet pas un fonctionnement en l'état de l'application  

Notamment, dans de nombreux fichiers il est nécessaire de remplacer les login/mots de passe, n° de ports par des valeurs appropriées  


# ARCHITECTURE

ANGULAR  
 |  
 V  
JAVA -> MONGO_ATLAS (Cluster 1 -> Ids)  
 |  
 V  
NODE -> MONGO_ATLAS (Cluster 2 -> Donnees)  



# 2 - Architecture logicielle

## Mongo 
Utilisation des clusters de Mongo Cloud : cf https://cloud.mongodb.com/  
Création de la base de données  
Création des collections  

Cf JAVA pour les collections


## JAVA

### Connexion à la base de données
1. Déclaration dans le fichier properties :  
    Ajout de la chaine *mongodb+srv://<user>:<password>@<nomcluster>.<idgcp>.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority*  
    <user> est donné par SECURITY -> Database Access  
    <password> par SECURITY -> Database Access -> *Bouton EDIT* -> possiblité de regénérer un pwd auto  
    la chaine est récupérable depuis DATA STORAGE -> Clusters -> *Bouton CONNECT* -> Connect your application  

2. Ajout dans le pom.xml de la dependence qui regroupe tut ce qu'il faut pour Mongo :  
~~~ xml
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-mongodb</artifactId>
    </dependency>
~~~ 
3. Inutile / Ne pas avoir de classe DatabaseConfig.java avec @EnableMongoRepositories ...  
En revanche mettre absolument dans Main.java  *@EnableMongoRepositories(basePackages = {"projectnameWApp"})* Sinon @Autowired Repo ne fonctionnent plus


### Utilisation de profiles

1. Dans pom.xml ajout de différents profiles :   
~~~ xml
  <profiles>
    <profile>
      <id>dev</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <spring.profiles.active>dev</spring.profiles.active>
      </properties>
    </profile>
    <profile>
      <id>gcp</id>
      <properties>
        <spring.profiles.active>gcp</spring.profiles.active>
      </properties>
    </profile>
  </profiles>  
  ~~~ 
Ces profiles permettent de "fusionner" les propriétées issues de application.properties et application-*profile*.properties.  
Il faut bien respecter le - et le nom du profile  

Ajout dans application.properties de : spring.profiles.active=@spring.profiles.active@  
Les variables communes à tous les environnements peuvent rester dans ce fichier également  

**La compilation par maven doit préciser le profile utilisé, sinon il ne sera pas accessible lors du passage de paramètres à l'exécution :**
```bash
mvn clean install -P*profile*
```


Dans InteliJ, ajouter -Dspring.profiles.active=dev au niveau des VM options dans la configuration d'exécution

2. Le choix du profil à l'exécution s'effectue de la manière suivante :    
java -jar -Dspring.profiles.active=*profile* ./target/projectname.jar  


3. Création d'une classe spécifique AdditionalProperties 
~~~ java
@Configuration  
@ConfigurationProperties(prefix = "app")  
@Data  
public class AdditionalProperties {  
  private String node_url;  
}  
~~~

Cette classe permet de récupérer de application.properties nos propriétés spécifiques.  
Ainsi grâce à prefix = "app", tout attribut sera automatiquelent associé à l'attribut correspondant

4. En conjuguant 2. et 3. on peut ainsi définir des attributs spécifiques qui sont répartis dans des fichiers properties spécifiques à l'environnement  

5. Accessoirement, choix du profil pour l'exécution de mvn spring-boot:run 
~~~ xml
        <configuration>
          <profiles>
            <profile>dev</profile>
          </profiles>
        </configuration>
~~~

### Envoi de mail depuis le serveur  

0. cf https://cloud.google.com/compute/docs/tutorials/sending-mail/using-sendgrid?hl=fr
1. Souscription à Sendgrid.com.  
2. Configuration de email-API d'après les recommandations de sendgrid  
2.1. Ajout des noms DNS suivants dans Google Domain  

| Nom           | Type  |  TTL | Données (= Nom de domaine)                |  
|---------------|-------|------|-------------------------------------------|  
| @             | A     |  1h  | x.y.z.a                                   |  
| saucrate.fr   | A     |  1h  | x.y.z.a                                   |  
| www           | CNAME |  1h  | saucrate.fr                               |  
| s1.domainkey  | CNAME |  1h  | s1.domainkey.uXXXXXXXX.wYYYY.sendgrid.net |  
| s2.domainkey  | CNAME |  1h  | s2.domainkey.uXXXXXXXX.wYYYY.sendgrid.net |  
| emABCD        | CNAME |  1h  | uXXXXXXXX.wYYYY.sendgrid.net              |  

Attendre (parfois jusqu'à 48h !)  

3. Création d'un clé d'API avec acces restreints suivants :

| Access Detail   | Niveau d'accès |  
|-----------------|----------------|  
| API Keys        | Full           |  
| Email Activity  | Read           |  
| Email Testing   | Full           |  
| Mail send       | Full           |  
| Mail settings   | Full           |  

<span style="color: red">
Clé d'API (N'est plus accessible après validation) :   <br> 
Name: saucrate_app<br>
API Key ID: aBcDeFgHiJkLmNoPqRstuV  <br>  
value : SG.aBcDeFgHiJkLmNoPqRstuV.1aB2cD3eF4gH5iJ6kL7mN8oP9-qR0-sT1uV2wX3yZ4a    
</span>

4. Avant d'envoyer le premier e-mail, nécessité de créer un émetteur
Correspond au cas où la vérification de l'installation des DNS Records n'est pas vérifiée. Une fois les paramètres CNAME positionnés, il n'est plus demandé de créer un émetteur parce que le nom de domaine est vérifié par les fournisseurs d'accès

5. Envoyer des e-mails avec Java sur votre instance  
https://cloud.google.com/compute/docs/tutorials/sending-mail/using-sendgrid?hl=fr#javasendgrid  
Le code java est récupéré et intégré dans l'app PloTri : git clone https://github.com/GoogleCloudPlatform/java-docs-samples.git  
Copie de la classe java et ajout de la dépendance dans le fichier pom.xml
~~~xml
    <dependency>
      <groupId>com.sendgrid</groupId>
      <artifactId>sendgrid-java</artifactId>
      <version>4.6.4</version>
    </dependency>
~~~

6. Créer une variable d'environnement :
~~~bash
echo "export SENDGRID_API_KEY='YOUR_API_KEY'" > sendgrid.env
echo "sendgrid.env" >> .gitignore
source ./sendgrid.env
~~~


## Angular

Utilisation des fichiers environnements.ts et environnements.prod.ts pour définir des variables contextuelles aux environnements  


### Progressive Web App
ng add @angular/pwa --project ploTri
> Pour voir la progressive webapp fonctionner il faut qu'elle soit distribuée par un serveur
cf https://blog.angular-university.io/angular-service-worker/  

https://angular.io/guide/service-worker-intro#prerequisites : **In order for service workers to be registered, the app must be accessed over HTTPS, not HTTP. Browsers ignore service workers on pages that are served over an insecure connection. The reason is that service workers are quite powerful, so extra care needs to be taken to ensure the service worker script has not been tampered with. There is one exception to this rule: to make local development easier, browsers do not require a secure connection when accessing an app on localhost.**  
> ==> Nécessité d'utiliser 
> ```bash
> ng build --prod
> ```
> Sinon, on reste environnement de développement et le service Worker ne marche pas  
> Pour simuler l'environnement de production positionnement de *production: true* dans environment.localhostdockercompose.ts  


#### Installation de l'icône d'installation :
cf https://medium.com/@oleksandr.k/a-way-to-show-a-prompt-to-install-your-angular-pwa-both-on-android-and-ios-devices-7a770f55c54


##### Modifier les icônes associées à l'accès à l'application.
Dans ./01-IHM/src/assets/icons/, créer ses propres icones selon ce qui est décrit dans le fichier 01-IHM/src/manifest.webmanifest  

#### Tester si une nouvelle version existe
cf https://blog.angular-university.io/angular-service-worker/ Step 6 of 7 - Deploying a new Application Version, Understanding Version Management  
> constructor(private swUpdate: SwUpdate) {}
> ngOnInit() { if (this.swUpdate.isEnabled) { ...  


## Simulation d'un environnement sécurisé en environnement de développement

### 1 - Créer un certificat SSL auto-signé 
cf https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate



### 2 - Configurer le SSL sur NGINX
https://medium.com/faun/setting-up-ssl-certificates-for-nginx-in-docker-environ-e7eec5ebb418

1. Vérifer qu'un certificat correspond à une clé ou qu'un CSR correspond ) à un certificat :
```bash
openssl pkey -in server.key -pubout -outform pem | sha256sum
openssl x509 -in server.crt -pubkey -noout -outform pem | sha256sum
openssl req -in server.csr -pubkey -noout -outform pem | sha256sum
```

Les checksum doivent être identiques

2. Dans le docker-compose qui va monter le container, positionnement des ports sécurisés pour HTTPS.
> *Note : Also you need to know, HTTP listen from PORT:80 and HTTP(s) listen from 443*

Dans https_projectname.ihm-java-node.mongoCloud.yml, c'est au niveau de l'image de l'IHM qui contient également Angular  

3. Configurer l'utilisation des certificats
3.1. copier les fichiers certificats dans /etc/nginx/certs  
3.2. ajouetr les lignes ci-dessous dans le fichier nginx.conf

>    listen 443 ssl;
>    # En cohérence avec le nom du serveur entré dans le certificat
>    server_name domainname.fr;
>    ssl_certificate /etc/nginx/certs/your_site_crt_file.crt;
>    ssl_certificate_key /etc/nginx/certs/your_site_crt_file.key;



# 3 - Déploiement Kubernetes

## 3.1 - Création d'un projet sur GCP
https://console.cloud.google.com/

1. Créer un projet  
  Soit par Sélectionnez un projet -> Nouveau Projet
  Soit par Accéder à Compute Engine -> Créer Un Projet

2. Accéder à Kubernetes Engine
  ( Les modules sont activés)

3. Créer un Cluster
    Nom : projectname-cluster-1
    Zone : europe-west1-b
        Belgique. europe-west1-c, europe-west1-d y sont également
    Version maître : Version disponible -> Version standard
        Le choix de la version précoce expose au risque d'avoir des problèmes sans solution de contournement
    Série : N1 (Première génération)
    Type de Machine : g1-small (1 processeur, 1,7 Go RAM)
    Taille du disque de démarrage : 32 Go
    Activer l'autoscalling = false
    Activer Kubernetes Engine Monitoring = false  

4. Modifier le nombre de noeuds
     projectname-cluster-1 -> *Pools de noeuds -> default pool -> *Bouton MODIFIER*
        Taille = 1

## 3.2 - Création des images DOCKER

<span style="color:#ffb3b3;font-size:20px">
Le déploiement sous GCP s'effectue à l'aide du fichier build_global.sh
</span>  

```bash
cd .
. ./build_global.sh
```

### Préparation de l'environnement
Depuis un terminal sur la machine :

```bash
gcloud auth configure-docker
```

Depuis un terminal sur la machine :  
```bash
PROJECT_NAME='GCP-project-id'  
PROJECT_ID='project-id'  
REGION_ID='europe-west1'  
ZONE='europe-west1-b'  
IMAGE_IHM='plotri/project-id-ihm'
IMAGE_IHM_TAGGED=$IMAGE_IHM':v0'  
IMAGE_JAVA_APP='plotri/project-id-app-java'
IMAGE_JAVA_APP_TAGGED=$IMAGE_JAVA_APP':v0' 
IMAGE_NODE_APP='plotri/project-id-app-node'
IMAGE_NODE_APP_TAGGED=$IMAGE_NODE_APP':v0' 
gcloud config set project $PROJECT_ID  
gcloud config set run/region $REGION_ID  
gcloud config set compute/zone $ZONE  
gcloud config set compute/region $REGION_ID  
gcloud components update
```

### Image ANGULAR
1. Build de l'application Angular en mode production
2. Création de l'image Docker  
    Pour exposition locale  
    En mode prêt à déployer sur GCP  

### Image JAVA
Le Dockerfile contient le profile d'execution dans la ligne de commande de lancement du container :  
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=${EXEC_PROFILE}","app/projectname.jar","--spring.application.name=${SPRING_APPLICATION_NAME}"]

L'image est créée en précisant le profil utilisé :  
```bash  
mvn clean install -Pdev
docker build --rm=true --build-arg PROFILE=dev -t $IMAGE_JAVA_APP_TAGGED .   
```  

L'image est créée en précisant le profil utilisé (pour utilisation des containersavec docker-compose) :  
```bash 
mvn clean install -Plocalhostdockercompose
docker build --rm=true --build-arg PROFILE=localhostdockercompose -t $IMAGE_JAVA_APP_TAGGED .   
```  


Builder pour exposition locale avec la commande :
```bash
cd ./02-APP/01-Java/10_01_projectname_WEBAPP
# Build du projet java
echo "Build de l'application pour mongoAtlas sous forme de .jar"
mvn clean install -Pdev
docker build --rm=true --build-arg PROFILE=dev -t $IMAGE_JAVA_APP_TAGGED .  
```


### Image Node

Builder pour exposition locale avec la commande :
```bash
docker build --rm=true -t $IMAGE_NODE_APP_TAGGED . 
```


### Test en environnement local
```bash
cd .
docker-compose -f "./stack_projectname.ihm-java-node.mongoCloud.yml" up -d --build
echo "Arrêt"
docker-compose -f ./stack_projectname.ihm-java-node.mongoCloud.yml stop
```
On accède à l'IHM via http://localhost


## 3.3 - KUBERNETES

### 3.3.1 - Deployments

La création d'un déploiement permet de définir un pod.
Ce pod peut contenir plusieurs containers  
La configuration de ce déploiement conditionne son utilisation par les services définis par la suite  
Le déploiement contient le nom de l'/des image/s Docker à utiliser

Ici, création d'un d'un pod par image  
==> Création d'un fichier deployment par image docker copiée dans le hub eu.gcr.io

Dans l'environnement Gcloud ou en local si les lignes suivantes ou été exécutées

```bash
echo "Positionnement à l'environnement GCP Kubernetes"
gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE --project $PROJECT_ID
echo "Positionnement du cluster à 1 noeud"
echo "Y" | gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone=$ZONE
```

```bash
gcloud container clusters get-credentials projectname-cluster-1 --zone europe-west1-b --project project-id
kubectl apply -f projectname-deployment-appNodeAtlas.yaml
kubectl apply -f projectname-service-appNodeAtlas.yaml
kubectl apply -f lapaslsa-deployment-appJava.yaml
kubectl apply -f projectname-service-appJava.yaml
kubectl apply -f projectname-deployment-ihm.yaml
```

> Pour permettre aux pods Java et Node de consulter des bases de données Mongo, il faut autoriser l'adresse IP de GCP à accéder à MongoAtlas
> Ceci se fait de la manière suivante :
> 1. Récupérer l'adresse IP du Noeud
> soit par la commande et prendre la valeur EXTERNAL-IP
> ```bash
> kubectl get nodes --output wide
> NAME                                               STATUS   ROLES    AGE   VERSION           INTERNAL-IP   EXTERNAL-IP     OS-IMAGE                             KERNEL-VERSION   CONTAINER-RUNTIME
> gke-projectname-cluster-1-default-pool-05d198d0-md75   Ready    <none>   61m   v1.14.10-gke.36   10.a.b.c    35.d.e.f   Container-Optimized OS from Google   4.14.138+        docker://18.9.7
> ```
> soit en consultant l'adresse de type ExternalIP dans le YAML du noeud à partir de la console.

> 2. Déclarer cete adresse IP dans la white list de Mongo

### 3.3.2 - Services

La création d'un service par déploiement permet d'ouvrir un port du pod selon la configuration du service

1. Service IHM
2. Service App Java
3. Service App Node

### 3.3.3 - Exposition sur Internet

#### 3.3.3.1 - Exposition simple

1. Pour ce mode il faut changer **au préalable** dans le fichier projectname-service-ihm.yaml le type d'exposition de type = Node (utilisé par le service Ingress) par type = LoadBalancer  
2. <span style="color:red;font-size:12px">Ne permet pas de se connecter aux services parce que NGINX n'est pas configuré pour se rediriger vers les Back-ends.</span>  
Le mode Ingress qui va permettre le https est fait pour cela

Exposition de l'IHM : 
```bash
kubectl apply -f projectname-service-ihm.yaml
```

Regarder l'adresse IP fournie par EXTERNAL-IP
```bash
kubectl get services
```

Se connecter via http://EXTERNAL-IP:port où port est celui fourni dans le fichier yaml.

<span style="color:red;font-size:12px">
En cas d'erreur :
</span>  
Vérifier dans les fichier transmis (styles.xxx.css, )runtime-es.xxx.js, ...) que le href est bien cohérent avec la configuration nginx du conteneur


#### 3.3.3.2 - Ingresses

cf https://cloud.google.com/kubernetes-engine/docs/concepts/ingress  

> L'activation/déploiement d'un Ingress / Equilibrage de charge prend quelques minutes avant que ce ne soit effectif (d'apès la doc jusqu'à 10')  
> Illustration : le 14/07/2020 :   
> user@cloudshell:~ (project-id)$ kubectl get ingresses  