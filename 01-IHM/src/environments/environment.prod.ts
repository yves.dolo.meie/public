export const environment = {
  production: true,
  baseApiPath: window.location.protocol + '//' + window.location.hostname + '/api'
};
