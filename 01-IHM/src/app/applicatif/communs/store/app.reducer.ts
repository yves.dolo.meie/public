import {
    AppActions
} from './app.actions';

// tslint:disable-next-line: no-empty-interface
export interface State {
}

export const initialState: State = {
};

export function appReducer(state = initialState, action: AppActions): State {
    switch (action.type) {

        default:
            return state;
    }
}

