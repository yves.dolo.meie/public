import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, filter, tap, switchMap } from 'rxjs/operators';
import { Platform } from '@angular/cdk/platform';

import { MatDialog } from '@angular/material/dialog';
import { MsgBoxComponent } from '../pages/msg-box/msg-box.component';

import { AuthenticationService, AlertService, CheckServerService } from '../services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {


    isServeurDeconnecteNotifie = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);


    constructor(
        private authenticationService: AuthenticationService,
        private router: Router,
        private alertService: AlertService,
        private checkServerService: CheckServerService,
        public dialog: MatDialog,
        private platform: Platform) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let errorRet: any;

        return next.handle(request).pipe(
            // Partie sollicitée uniquement quand le statut est ok.
            // There may be other events besides the response.
            filter(event => event instanceof HttpResponse),
            tap((event: HttpResponse<any>) => {
                //                console.log('event interceptor : ', event);
                this.serveurJoignable();
            })
            ,
            catchError(err => {
                // console.log('Error interceptor : ', err);
                // HTTP 401 ==> Non authentifié
                if (err.status === 401) {
                    return this.gererRenouvellementTocken(err, request, next);
                }
                else if (err.status === 422) {
                    this.alertService.error('422');
                }
                else if (err.status === 403) {
                    // HTTP 403 ==> Non autorisé
                    this.alertService.error('403');
                    // this.authenticationService.logout();
                    // location.reload();
                    this.router.navigate(['/']);
                }
                else {
                    if (err && err.ok) {
                        this.serveurJoignable();

                        // HTTP 500 ==> Erreur serveur - comme crash de node
                        if (err.status === 500) {
                            this.alertService.error('500');
                        }

                        errorRet = err.error ? err.error.message : err.statusText;

                    } else {
                        // cas ou err.ok = KO ==> Le back-end est dans un état instable

                        // err.status = 0 => serveur injoignable
                        if (err.status === 0) {
                            this.serveurNonJoignable();
                        }

                        else {
                            console.log('Error interceptor : ', err);
                            this.serveurJoignable();
                            this.erreurIndeterminee();
                        }
                        errorRet = 'Erreur indéterminée !';
                    }
                }
                return throwError(errorRet);
            })
        );
    }

    gererRenouvellementTocken(err, request: HttpRequest<any>, next: HttpHandler) {
        // Cas du tocken expiré
        if (err.error.code === 40) {
            this.refreshTokenSubject.next(null);
            //            console.log('étape 1', request, next);
            if (this.platform.ANDROID || this.platform.IOS) {
                //                console.log('étape 2');
                return this.authenticationService.reauth().pipe(
                    switchMap(token => {
                        //                        console.log('%c %s', 'background: #222; color: #bada55',  token);
                        this.refreshTokenSubject.next(token);
                        //                        console.log('%c %s', 'background: #4444; color: blue',  token);
                        return next.handle(this.addToken(request, token));
                    }));
            } else {
                //                console.log('étape 4');
                this.authenticationService.logout();
                location.reload();
                this.router.navigate(['/']);
                return throwError('Le renouvellement du token ne s\'applique pas');
            }
        }
        if (err.error.code === 60) {
            return throwError('401');
        } else {
            this.serveurJoignable();
            this.erreurIndeterminee();
            return throwError('Erreur indéterminée !');
        }
    }


    erreurIndeterminee() {
        this.alertService.error('-1');
        this.dialog.open(MsgBoxComponent, {
            data: {
                iconType: 'error_outline',
                iconColor: 'red',
                message: 'Une erreur indéterminée est survenue.\nSi celle-ci persiste veuillez contacter l\'administrateur du site'
            }
        });
        setTimeout(() => {
            this.dialog.closeAll();
            this.router.navigate(['/']);
        }
            , 2500);
    }



    serveurNonJoignable() {
        this.checkServerService.error('0');
        if (!this.isServeurDeconnecteNotifie) {
            this.isServeurDeconnecteNotifie = true;

            this.dialog.open(MsgBoxComponent, {
                data: {
                    iconType: 'error_outline',
                    iconColor: 'red',
                    message: 'L\'application fonctionne en mode déconnectée.\n Aucune modification ne peut être prise actuellement en compte'
                }
            });
            setTimeout(() => {
                this.dialog.closeAll();
            }
                , 2500);
        }
    }

    serveurJoignable() {
        this.isServeurDeconnecteNotifie = false;
        this.checkServerService.success('HTTP200');
    }


    private addToken(request: HttpRequest<any>, token: string) {
        //        console.log(token);
        return request.clone({
            setHeaders: {
                Authorization: `Bearer ${token}`
            }
        });
    }
}
