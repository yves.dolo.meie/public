import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { Observable, throwError } from 'rxjs';
import { AppConfig } from '../../../config';

import { HttpHeaders } from '@angular/common/http';

import { catchError } from 'rxjs/operators';

export class PathVariable {
    key: string;
    value: string;

    constructor(key: string, value: string) {
        this.key = key;
        this.value = value;
    }
}

@Injectable()
export class ApiRequestService {

    private handleError: HandleError;

    httpOptions: any;

    constructor(
        private http: HttpClient,
        private httpErrorHandler: HttpErrorHandler,
        private appConfig: AppConfig
    ) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.handleError = this.httpErrorHandler.createHandleError('ApiRequestService');
        // console.log("YDO **** ApiRequestService  constructor" + JSON.stringify(this.httpOptions));
    }

    setOptions(params: HttpParams): any {
        // console.log('j utilise setOptions de api-request');
        let result: any;
        // Le seul cas où le tocken n'est pas positionné est celui de sa récupération via la méthode POST
        result = { params, headers: this.httpOptions.headers };
        return result;

    }

    get(url: string, urlParams?: PathVariable[], headersParams?: any): Observable<any> {

        url = this.appConfig.baseApiPath + url;
        if (urlParams) {
            urlParams.forEach(v => url = url.replace('{' + v.key + '}', v.value));
        }
        // console.log('\n\n URL = %s \n urlParams = \n headersParams =\n', url, urlParams, headersParams);
        const options = this.setOptions(headersParams);
        return this.http.get(url, options)
            .pipe(catchError(err => {
                catchError(this.handleError(url, 'get'));
                return throwError(err);
            }));
    }

    post(url: string, body: Object, headersParams?: any): Observable<any> {
        url = this.appConfig.baseApiPath + url;
        const options = this.setOptions(headersParams);
//        console.log(url);
        return this.http.post<Object>(url, body, options)
            .pipe(catchError(err => {
                this.handleError(url, body);
                // err retourne ici OK / undefined mais pas le status 200,, 401, 404 ...
                return throwError(err);
            }));
    }

}
