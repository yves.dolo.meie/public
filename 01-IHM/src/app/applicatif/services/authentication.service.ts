import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Platform } from '@angular/cdk/platform';

import { ApiRequestService } from './apiRequest/api-request.service';

import { User } from '../../metier/modele';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    private readonly JWT_TOKEN = 'JWT_TOKEN';
    private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';

    constructor(
        private apiRequest: ApiRequestService,
        private platform: Platform) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username, password): Observable<any> {
        /*
        return this.http.post<any>(`${config.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
*/

        const lasSubject: Subject<any> = new Subject<any>();

        const audience = 'mobile';
        const params = this.platform.ANDROID || this.platform.IOS ? { username, password, audience } : { username, password };

        // console.log(this.platform, params); // Même en PWA, isBrowser = true

        this.apiRequest.post('/auth', params)
            .subscribe(user => {
                // A mettre absolument dans subscribe. S'il est en dehors,
                // l'asynchronisme fera que l'affectation sera réalisée alors que la réponse ne sera pas obtenue
                this.currentUserSubject.next(user);
                localStorage.setItem('currentUser', JSON.stringify(user));
                lasSubject.next(user);
//                console.log('AuthenticationService : ok', this.currentUser);
            },
                err => {
//                    console.log('AuthenticationService : err = ', err);
                    lasSubject.error(err);
                });
        return lasSubject;

    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }



    sendLinkForNewPassword(mail): Observable<any> {
        const lasSubject: Subject<any> = new Subject<any>();
        this.apiRequest.post('/newpassword', mail)
            .subscribe(resp => {
                lasSubject.next(resp);
            },
                err => {
//                    console.log('AuthenticationService : err = ', err);
                    lasSubject.error(err);
                });
        return lasSubject;

    }

    resetpassword(tocken, password): Observable<any> {
        const lasSubject: Subject<any> = new Subject<any>();
        this.apiRequest.post('/resetpassword', { tocken, password })
            .subscribe(resp => {
                lasSubject.next(resp);
            },
                err => {
//                    console.log('AuthenticationService.resetPwd : err = ', err);
                    lasSubject.error(err);
                });
        return lasSubject;

    }


    /**
     * En cliquant sur le lien, l'utilisateur confirme qu'il est à l'origine de la création du compte
     * @param token : token de confirmation
     */
    confirmAccount(token): Observable<any> {
        const lasSubject: Subject<any> = new Subject<any>();
        this.apiRequest.post('/confirmaccount', token)
            .subscribe(resp => {
                lasSubject.next(resp);
            },
                err => {
//                    console.log('AuthenticationService : err = ', err);
                    lasSubject.error(err);
                });
        return lasSubject;

    }

    reauth(): Observable<any> {
        const authSubject: Subject<any> = new Subject<any>();
        const currentUser = this.currentUserValue;
        const token = currentUser.token;
        const refreshToken = currentUser.refreshToken;
        currentUser.token = null; // Pour forcer la mise à jour du tocken et bypasser l'interception du back-end

        this.apiRequest.post('/reauth', { token, refreshToken })
            .subscribe(resp => {
                // A mettre absolument dans subscribe. S'il est en dehors,
                // l'asynchronisme fera que l'affectation sera réalisée alors que la réponse ne sera pas obtenue
                currentUser.token = resp.token;
                currentUser.refreshToken = resp.refreshToken;
                this.currentUserSubject.next(currentUser);
                localStorage.setItem('currentUser', JSON.stringify(currentUser));
                authSubject.next(resp.token);
                // console.log('AuthenticationService : ok', this.currentUser);
            },
                err => {
//                    console.log('AuthenticationService : err = ', err);
                    authSubject.error(err);
                });
        return authSubject;

    }


}

