import { Injectable } from '@angular/core';


import { ApiRequestService } from './apiRequest/api-request.service';
import { Observable, Subject } from 'rxjs';


import { User } from '../../metier/modele';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  user: User;

  constructor(private apiRequest: ApiRequestService) { }


  register(user: User): Observable<User[]> {
    // Utilisé pour laisser le temps au service de répondre avant que la page ne se charge
    const registersSubject: Subject<any> = new Subject<any>();

    this.apiRequest.post('/register', user)
      .subscribe(jsonResp => {
        // console.log("this.apiRequest.get('deflauation', {params: new HttpParams() ...", jsonResp)
        registersSubject.next(jsonResp);
      });
    return registersSubject;
  }


}
