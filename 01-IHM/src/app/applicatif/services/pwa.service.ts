import { Injectable } from '@angular/core';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { PromptComponent } from '../pages';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { Platform } from '@angular/cdk/platform';

@Injectable({
  providedIn: 'root'
})
export class PwaService {
  private promptEvent: any;
  private timer = 3000;

  constructor(
    private bottomSheet: MatBottomSheet,
    private platform: Platform
  ) { }

  initPwaPrompt() {
    if (this.platform.ANDROID) {
      window.addEventListener('beforeinstallprompt', (event: any) => {
        event.preventDefault();
        this.promptEvent = event;
        this.openPromptComponent('android');
      });
    }
    if (this.platform.IOS) {
      const isInStandaloneMode = ('standalone' in window.navigator) && (window.navigator['standalone']);
      if (!isInStandaloneMode) {
        this.openPromptComponent('ios');
      }
    }
  }

  openPromptComponent(mobileType: 'ios' | 'android') {
    timer(this.timer)
    .pipe(take(1))
    .subscribe(() => {
      this.timer = 20000; // attente de 20s pour le prochain prompt
      this.bottomSheet.open(PromptComponent, { data: { mobileType, promptEvent: this.promptEvent } });
    });
  }
}
