export class Consentement {
    quand: Date;
    isBoutonComprisUtilise: boolean;
    isSauvegarde: boolean; // pour savoir s'il faut resynchroniser parce que non loggué ou déconnecté
}