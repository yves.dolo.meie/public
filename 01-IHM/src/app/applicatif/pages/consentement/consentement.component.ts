import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface DialogData {
  isConditionsComprises: boolean;
}

@Component({
  selector: 'app-consentement',
  templateUrl: './consentement.component.html',
  styleUrls: ['./consentement.component.css']
})
export class ConsentementComponent implements OnInit {

  public laPasla = 'Là Pas là !';

  constructor(
    public dialogRef: MatDialogRef<ConsentementComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onClick() {
    this.data.isConditionsComprises = true;
    this.dialogRef.close(this.data);
  }

  ngOnInit(): void {
  }

}
