import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';

import { AuthenticationService } from '../../services';

import { MatDialog } from '@angular/material/dialog';
import { MsgBoxComponent } from '../msg-box/msg-box.component';


@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent implements OnInit {
  loading = false;
  token: string;
  COMPTE_CLIENT_CONFIRME = 'La confirmation de votre identité a bien été prise en compte';
  ERREUR = 'Une erreur est survenue, veuillez réessayer ultérieurement ou contacter le support';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      this.token = paramMap.get('token');
      this.confirmAccount();

    });
  }

  confirmAccount() {
    this.authenticationService.confirmAccount(this.token)
      .subscribe(
        data => {
          this.showAlert(this.COMPTE_CLIENT_CONFIRME, 'check_circle_outline', 'green');
          this.loading = false;
        },
        error => {
          this.showAlert(this.ERREUR, 'error_outline', 'red');
          this.loading = false;
        });
  }



  showAlert(msg, iType, iColor) {
    this.dialog.open(MsgBoxComponent, {
      data: {
        iconType: iType,
        iconColor: iColor,
        message: msg
      }
    });
    setTimeout(() => {
      this.dialog.closeAll();
      this.router.navigate(['/']);
    }
      , 4000);

  }
}
