import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';

import { AuthenticationService } from '../../services';

import { MatDialog } from '@angular/material/dialog';
import { MsgBoxComponent } from '../msg-box/msg-box.component';

@Component({
  selector: 'app-pwd-reset',
  templateUrl: './pwd-reset.component.html',
  styleUrls: ['./pwd-reset.component.css']
})
export class PwdResetComponent implements OnInit {

  public isVisible = false;
  submitted = false;
  returnUrl: string;
  hide = true;
  error = '';

  loading = false;
  tocken: string;
  PWD_MODIFIE = 'Votre nouveau mot de passe a bien été pris en compte';
  ERREUR = 'Une erreur est survenue, veuillez réessayer ultérieurement ou contacter le support';

  pwdCtrl = new FormControl(null, [Validators.required, Validators.minLength(8)]);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      this.tocken = paramMap.get('token');
    });
  }



  async onSubmit() {
    this.submitted = true;


    this.loading = true;
    // console.log('Login %s, pwd %s', this.pwdCtrl.value, this.pwdCtrl.value);

    this.authenticationService.resetpassword(this.tocken, this.pwdCtrl.value)
      .subscribe(
        data => {
          this.showAlert(this.PWD_MODIFIE, 'check_circle_outline', 'green');
          this.loading = false;
        },
        error => {
          this.showAlert(this.ERREUR, 'error_outline', 'red');
          this.loading = false;
        });
  }



  showAlert(msg, iType, iColor) {
    this.dialog.open(MsgBoxComponent, {
      data: {
        iconType: iType,
        iconColor: iColor,
        message: msg
      }
    });
    setTimeout(() => {
      this.dialog.closeAll();
      this.router.navigate(['/']);
    }
      , 4000);

  }

}
