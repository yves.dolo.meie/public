import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';


import { AuthenticationService } from '../../services';

import { MatDialog } from '@angular/material/dialog';
import { MsgBoxComponent } from '../msg-box/msg-box.component';

@Component({
  selector: 'app-pwd-forgot',
  templateUrl: './pwd-forgot.component.html',
  styleUrls: ['./pwd-forgot.component.css']
})
export class PwdForgotComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  hide = true;
  error = '';

  alertMsg = '';
  MAIL_ENVOYE = 'Si votre adresse est connue, un mail vient de vous être adressé pour réinitialiser votre mot de passe';
  ERREUR = 'Une erreur est survenue, veuillez réessayer ultérieurement ou contacter le support';

  mailCtrl = new FormControl(null, [Validators.required, Validators.email]);

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit(): void {
  }


  async onSubmit() {
    /*
    const digestBuffer = await digestMessage(this.pwdCtrl.value);
    console.log(digestBuffer);
    console.log(digestBuffer.byteLength);
    */
    this.submitted = true;


    this.loading = true;
    // console.log('Login %s, pwd %s', this.mailCtrl.value, this.pwdCtrl.value);

    this.authenticationService.sendLinkForNewPassword(this.mailCtrl.value)
      .subscribe(
        data => {
          console.log('ICI');
          this.showAlert(this.MAIL_ENVOYE, 'check_circle_outline', 'green');
          this.loading = false;
        },
        error => {
          this.showAlert(this.ERREUR, 'error_outline', 'red');
          this.loading = false;
        });
  }

 
  showAlert(msg, iType, iColor) {
    this.dialog.open(MsgBoxComponent, {
        data: {
            iconType: iType ,
            iconColor: iColor,
            message: msg
        }
    });
    setTimeout(() => {
        this.dialog.closeAll();
        this.router.navigate(['/']);
    }
        , 4000);
}


}
