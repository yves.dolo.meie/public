import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';

import { AlertService, AuthenticationService } from '../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  hide = true;
  error = '';

  loginCtrl = new FormControl(null, [Validators.required, Validators.email]);
  pwdCtrl = new FormControl(null, [Validators.required]);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.alertService.getAlert().subscribe(alert => this.traiterAlerte(alert));

  }


  async onSubmit() {
    /*
    const digestBuffer = await digestMessage(this.pwdCtrl.value);
    console.log(digestBuffer);
    console.log(digestBuffer.byteLength);
    */
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    this.loading = true;
    // console.log('Login %s, pwd %s', this.loginCtrl.value, this.pwdCtrl.value);

    this.authenticationService.login(this.loginCtrl.value, this.pwdCtrl.value)
      .subscribe(
        data => {
          this.router.navigate(['/']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  traiterAlerte(alert) {
    if (alert) {
      switch (alert.text) {
        case '401':
          this.error = 'Identifiant ou mot de passe non reconnu.\nVérifiez votre saisie et essayez à nouveau';
          this.loginCtrl.setValue(''); this.loginCtrl.reset();
          this.pwdCtrl.setValue(''); this.pwdCtrl.reset();
          break;
        default:
          break;
      }
    }

  }

}

