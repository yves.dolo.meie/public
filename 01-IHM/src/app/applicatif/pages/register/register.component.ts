import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl } from '@angular/forms';

import {MatDialog} from '@angular/material/dialog';
import { MsgBoxComponent } from '../msg-box/msg-box.component';

import { AlertService, RegisterService } from '../../services';
import { User } from '../../../metier/modele';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  hide = true;
  isFormValid = false;
  error = '';

  loginCtrl = new FormControl(null, [Validators.required, Validators.email]);
  pseudoCtrl = new FormControl(null);
  nomCtrl = new FormControl(null, [Validators.required]);
  prenomCtrl = new FormControl(null, [Validators.required]);
  pwdCtrl = new FormControl(null, [Validators.required, Validators.minLength(8)]);
  enregistrerCtrl = new FormControl();

  constructor(
    private router: Router,
    private registerService: RegisterService,
    private alertService: AlertService,
    public dialog: MatDialog
  ) { }

  loading = false;
  public isButtonPressed = false;

  ngOnInit(): void {
    this.alertService.getAlert().subscribe(alert => this.traiterAlerte(alert));
  }

  onSubmit() {

    if (!(this.nomCtrl.invalid || this.prenomCtrl.invalid || this.loginCtrl.invalid || this.pwdCtrl.invalid)) {
      this.isButtonPressed = true;
      this.loading = true;
      

      const user = new User();
      user.firstName = this.prenomCtrl.value;
      user.lastName = this.nomCtrl.value;
      user.login = this.loginCtrl.value;
      user.password = this.pwdCtrl.value;
      user.pseudo = this.pseudoCtrl.value;

      this.registerService.register(user).subscribe(ok => {
        this.showAlert();
        this.loading = false;
      });
    }
  }


  showAlert() {
    this.dialog.open(MsgBoxComponent, {
      data: {
        iconType: 'check_circle_outline',
        iconColor: 'green',
        message: 'Votre compte a bien été créé'
      }
    });
    setTimeout(() => {
      this.dialog.closeAll();
      this.router.navigate(['/']);
    }
      , 2500);
  }

  traiterAlerte(alert) {
    if (alert) {
      switch (alert.text) {
        case '422':
          console.log('alerte :', alert);
          this.error = 'Le compte n\'a pas pu être créé.\nL\'identifiant ou le mot de passe est invalide';
          this.loading = false;
          this.loginCtrl.setValue(''); this.loginCtrl.reset();
          this.pwdCtrl.setValue(''); this.pwdCtrl.reset();
          this.isButtonPressed = false;
          break;
        default:
          break;
      }
    }
  }


}
