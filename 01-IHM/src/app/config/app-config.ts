import { Injectable } from '@angular/core';
import { HeureSport } from '../metier/modele';

import { environment } from '../../environments/environment';

/**
 * This is a singleton class
 */
@Injectable()
export class AppConfig {
    // Provide all the Application Configs here

    public version = '0.0.1';
    public locale = 'fr-FR';
    public currencyFormat = { style: 'currency', currency: 'EUR' };
    public dateFormat = { year: 'numeric', month: 'short', day: 'numeric' };

    public mapDaysSports: Map<number, HeureSport[]> = new Map();

    public IDAFF = 'IDAFF';

    public baseApiPath = environment.baseApiPath;

    constructor() {
        this.mapDaysSports.set(0, [new HeureSport(this.IDAFF + '0', 'V', '09:00', true, 0, 0)]);
        this.mapDaysSports.set(1, [new HeureSport(this.IDAFF + '1', 'C', '19:00', true, 0, 0)]);
        this.mapDaysSports.set(2,
            [new HeureSport(this.IDAFF + '2', 'N', '09:45', true, 0, 0),
            new HeureSport(this.IDAFF + '3', 'N', '19:30', true, 0, 1)]);
        this.mapDaysSports.set(4, [new HeureSport(this.IDAFF + '4', 'C', '19:00', true, 0, 0)]);
        this.mapDaysSports.set(5, [new HeureSport(this.IDAFF + '5', 'N', '19:15', true, 0, 0)]);
        this.mapDaysSports.set(6, [new HeureSport(this.IDAFF + '6', 'N', '18:00', true, 0, 0)]);

    }
}
