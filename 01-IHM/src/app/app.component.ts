import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Router } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { ConsentementComponent } from './applicatif/pages/consentement/consentement.component';
import { Consentement } from './applicatif/model';

import { AuthenticationService, CheckServerService } from './applicatif/services';
import { User } from './metier/modele';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser: User;
  title = 'LaPasLa';
  isFooterConstementVisible = true;
  isFooterModeDeconnecteVisible = false;

  consentement: Consentement;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private swUpdate: SwUpdate,
    public dialog: MatDialog,
    private checkServerService: CheckServerService,
  ) {
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;
      // console.log('AppComponent authenticationService.currentUser.subscribe', this.currentUser);
    });
  }

  ngOnInit() {
    this.checkServerService.getAlert().subscribe(alert => this.traiterAlerte(alert));

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('Une nouvelle version est disponible. Voulez-vous l\'installer ?')) {
          window.location.reload();
        }
      });
    }


    this.verifierConstentement();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }


  onMentionLegales(): void {
    const dialogRef = this.dialog.open(ConsentementComponent, {
      direction: "ltr",
      data: { isConditionsComprises: false }
    });
    dialogRef.afterClosed().subscribe(result => {
//      console.log(result);
      if (result) {
        this.sauverConstentement(result.isConditionsComprises);
      }
    });
  }


  sauverConstentement(isBoutonComprisUtilise) {
    console.log(isBoutonComprisUtilise);
    this.isFooterConstementVisible = false;
    this.consentement = new Consentement();
    this.consentement.isBoutonComprisUtilise = isBoutonComprisUtilise;
    this.consentement.quand = new Date();

    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser) {
      console.warn('TO DO : sauvegarder le consentement');
    } else {
      this.consentement.isSauvegarde = false;
    }

    localStorage.setItem('consentement', JSON.stringify(this.consentement));

  }

  verifierConstentement() {
    this.consentement = JSON.parse(localStorage.getItem('consentement'));
    if (this.consentement) {
      this.isFooterConstementVisible = false;
    }
  }


  traiterAlerte(alert) {
//    console.log('Alerte AppComponent', alert);
    if (alert) {
      switch (alert.text) {
        case '0':
          this.isFooterModeDeconnecteVisible = true;
          break;
        case 'HTTP200':
          this.isFooterModeDeconnecteVisible = false;
          break;
        default:
          break;
      }
    }
  }



}
