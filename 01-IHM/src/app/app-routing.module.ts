import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmAccountComponent } from './applicatif/pages';
import { DeleteAccountComponent } from './applicatif/pages';
import { HomeComponent } from './applicatif/pages';
import { LaPasLaComponent } from './metier/pages';
import { LoginComponent } from './applicatif/pages';
import { MentionsLegalesComponent } from './applicatif/pages';
import { ModeEmploiComponent } from './metier/pages';
import { PwdForgotComponent } from './applicatif/pages';
import { PwdResetComponent } from './applicatif/pages';
import { RegisterComponent } from './applicatif/pages';

import { AuthGuard } from './applicatif/communs';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'confirmAccount/:token', component: ConfirmAccountComponent },
  { path: 'deleteAccount/:token', component: DeleteAccountComponent },
  { path: 'la', component: LaPasLaComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'mentionslegales', component: MentionsLegalesComponent },
  { path: 'tuto', component: ModeEmploiComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'requestpassword', component: PwdForgotComponent },
  { path: 'resetpassword/:token', component: PwdResetComponent },


  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
