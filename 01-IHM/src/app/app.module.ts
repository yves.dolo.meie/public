import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule } from '@angular/platform-browser';
import { ReactiveFormsModule} from '@angular/forms';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreModule } from '@ngrx/store';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule} from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

import { ScrollingModule} from '@angular/cdk/scrolling';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './metier/pages/home/home.component';
import { LoginComponent } from './applicatif/pages/login/login.component';
import { RegisterComponent } from './applicatif/pages/register/register.component';

// Services
import { ApiRequestService } from './applicatif/services/apiRequest/api-request.service';
import { HttpErrorHandler } from './applicatif/services/apiRequest/http-error-handler.service';
import { MessageService } from './applicatif/services/apiRequest/message.service';


import { LaService } from './metier/services/la.service';
import { PwaService } from './applicatif/services/pwa.service';

import { AppConfig } from './config/app-config';


// used to create fake backend
import { JwtInterceptor, ErrorInterceptor } from './applicatif/communs';
import { LaPasLaComponent } from './metier/pages/la-pas-la/la-pas-la.component';
import { DateTabsComponent } from './metier/pages/date-tabs/date-tabs.component';
import { ChoixActiviteComponent } from './metier/pages/choix-activite/choix-activite.component';
import { MyHammerConfig } from './applicatif/communs/my-hammer-config';
import { reducers } from './applicatif/communs/store/index';
import { PwdForgotComponent } from './applicatif/pages/pwd-forgot/pwd-forgot.component';
import { PwdResetComponent } from './applicatif/pages/pwd-reset/pwd-reset.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { PromptComponent } from './applicatif/pages/prompt/prompt.component';


import { environment } from '../environments/environment';
import { MsgBoxComponent } from './applicatif/pages/msg-box/msg-box.component';
import { ConsentementComponent } from './applicatif/pages/consentement/consentement.component';
import { MentionsLegalesComponent } from './applicatif/pages/mentions-legales/mentions-legales.component';
import { ModeEmploiComponent } from './metier/pages/mode-emploi/mode-emploi.component';
import { ConfirmAccountComponent } from './applicatif/pages/confirm-account/confirm-account.component';
import { DeleteAccountComponent } from './applicatif/pages/delete-account/delete-account.component';

const initializer = (pwaService: PwaService) => () => pwaService.initPwaPrompt();


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    LaPasLaComponent,
    DateTabsComponent,
    ChoixActiviteComponent,
    PwdForgotComponent,
    PwdResetComponent,
    PromptComponent,
    MsgBoxComponent,
    ConsentementComponent,
    MentionsLegalesComponent,
    ModeEmploiComponent,
    ConfirmAccountComponent,
    DeleteAccountComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ScrollingModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSortModule,
    MatTabsModule,
    MatTableModule,
    MatToolbarModule,
    HammerModule,
    StoreModule.forRoot(reducers),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
   ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: initializer, deps: [PwaService], multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig },
    AppConfig,
    ApiRequestService,
    LaService,
    HttpErrorHandler,
    MessageService
  ],
  entryComponents: [
    PromptComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
