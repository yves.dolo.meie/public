import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { LaService } from './la.service';
import { catchError, finalize } from 'rxjs/operators';
import { Observable, BehaviorSubject, of } from 'rxjs';

import { La } from '../modele';


export class LaDataSource implements DataSource<La> {

  private lasSubject = new BehaviorSubject<La[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private laService: LaService) {

  }


  loadAllLas() {

    this.loadingSubject.next(true);

    this.laService.getLas().pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(las => {
        this.lasSubject.next(las);
        this.loadingSubject.next(false);
      }
      );
  }

  loadLas(
    invertedfilter: Map<string, string[]>, // filtre inversé. Si un élément est présent c'est pour le retirer de la recherche
    sortDirection: string,
    sortCriteria: string,
    pageIndex: number,
    pageSize: number) {

    this.loadingSubject.next(true);

    this.laService.getLas().pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(las => {
        this.lasSubject.next(las);
        this.loadingSubject.next(false);
      }
      );
  }

  connect(collectionViewer: CollectionViewer): Observable<La[]> {
    console.log('Connecting data source --> disconnecting ?');
    return this.lasSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    console.log('disconnect data source');
    this.lasSubject.complete();
    this.loadingSubject.complete();
  }

}
