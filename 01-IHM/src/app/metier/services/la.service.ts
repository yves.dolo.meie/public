import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ApiRequestService } from '../../applicatif/services';
import { Observable, Subject } from 'rxjs';


import { La } from '../modele';


@Injectable({
  providedIn: 'root'
})
export class LaService {


  // Observable ==> file contenant les messages véhiculés entre les emmeteurs et listeElementsSelectionnables
  private LaSubject = new Subject<La>();
  // Observable POJO  streams
  LaObservable$ = this.LaSubject.asObservable();



  constructor(
    private apiRequest: ApiRequestService) { }


  getLas(): Observable<La[]> {
    // Utilisé pour laisser le temps au service de répondre avant que la page ne se charge
    const lasSubject: Subject<any> = new Subject<any>();
    let las: La[];

    this.apiRequest.get('/la', null)
      // FIN YDO 2018 09 25
      .subscribe(jsonResp => {
        // console.log("this.apiRequest.get('deflauation', {params: new HttpParams() ...", jsonResp)
        las = jsonResp;
        // A mettre absolument dans subscribe. S'il est en dehors,
        // l'asynchronisme fera que l'affectation sera réalisée alors que la réponse ne sera pas obtenue
        lasSubject.next(las);
      });
    return lasSubject;
  }


  getLasFromDate(sDate: string): Observable<La[]> {
    const lasSubject: Subject<any> = new Subject<any>();

    const params = new HttpParams()
      .set('d', sDate);

    this.apiRequest.get('/la', null, params)
      .subscribe(las => {
        lasSubject.next(las);
      });
    return lasSubject;
  }


  postLa(la: La): Observable<La> {
    const lasSubject: Subject<any> = new Subject<any>();
    this.apiRequest.post('/la', la)
      .subscribe(laResp => {
        lasSubject.next(laResp);
      });
    return lasSubject;
  }

}
