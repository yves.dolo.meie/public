﻿export class User {
    id: string;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    pseudo: string;
    token: string;
    refreshToken: string;
}
