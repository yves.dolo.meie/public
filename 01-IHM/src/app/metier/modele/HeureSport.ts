export class HeureSport {
    activite: string; // N = Natation, V=Vélo, C=Course à pied
    heure: string; // format HH:MM
    horaireFige = false;
    choisi: number;
    ordreAffichage: number; // ordre d'affichage
    id: string;
    idAff: string; // identifiant interne à l'affichage

    // tslint:disable-next-line: max-line-length
    constructor(idAff: string, activite: string, heure: string, horaireFige?: boolean, choisi?: number, ordreAffichage?: number, id?: string) {
        this.idAff = idAff;
        this.activite = activite;
        this.heure = heure;
        this.horaireFige = horaireFige;
        this.choisi = choisi;
        this.ordreAffichage = ordreAffichage;
        this.id = id;
    }

    clone(): HeureSport {
        return new HeureSport(
            this.idAff,
            this.activite,
            this.heure,
            this.horaireFige,
            this.choisi,
            this.ordreAffichage,
            this.id
        );

    }
}
