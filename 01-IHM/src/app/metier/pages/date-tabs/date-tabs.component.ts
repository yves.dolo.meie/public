import { Component, OnInit, OnDestroy, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { ActionsSubject, Action } from '@ngrx/store';
import { takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AppActionTypes } from '../../../applicatif/communs/store/app.actions';

import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { AppConfig } from '../../../config';


@Component({
  selector: 'app-date-tabs',
  templateUrl: './date-tabs.component.html',
  styleUrls: ['./date-tabs.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class DateTabsComponent implements OnInit, OnDestroy {
  @Input() dateInit: Date;
  @Output() dateSelectionnee = new EventEmitter<Date>();

  private ngUnsubscribe = new Subject<void>();

  b1selected = true;
  b1label = ''; //b1label = new Date();
  b1value = new Date();
  b2label = '';
  b2value = new Date();
  label = 0;
  currentMonth = '';
  dateCtrl = new FormControl();

  /**
   * Le dernier traitement appelé doit TOUJOURS ETRE  setCurrentMonth()
   */

  constructor(
    private datePipe: DatePipe,
    private appConfig: AppConfig,
    private actionsSubject: ActionsSubject) { }

  ngOnInit(): void {
    this.b1label = this.datePipe.transform(this.dateInit, 'dd MMM');
    this.b2value.setDate(this.b1value.getDate() + 1);
    this.b2label = this.datePipe.transform(this.b2value, 'dd MMM');
    this.dateCtrl.setValue(this.b1value);
    this.setCurrentMonth();

    this.actionsSubject
      .pipe(
        filter((action: Action) => {
          return (
            action.type === AppActionTypes.SwipeLeft ||
            action.type === AppActionTypes.SwipeRight
          );
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(action => {
        if (action.type === AppActionTypes.SwipeLeft) {
          this.next();
        } else if (action.type === AppActionTypes.SwipeRight) {
          this.previous();
        }
      });

  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onB1selected() {
    this.b1selected = true;
    this.dateCtrl.setValue(this.b1value);
    this.setCurrentMonth();


  }
  onB2selected() {
    this.b1selected = false;
    this.dateCtrl.setValue(this.b2value);
    this.setCurrentMonth();
  }

  next() {
    this.setDates(1);
    this.setCurrentMonth();
  }

  previous() {
    this.setDates(-1);
    this.setCurrentMonth();
  }

  setDates(i) {
    this.b1value.setDate(this.b1value.getDate() + i);
    this.b1label = this.datePipe.transform(this.b1value, 'dd MMM');
    this.b2value.setDate(this.b2value.getDate() + i);
    this.b2label = this.datePipe.transform(this.b2value, 'dd MMM');
    this.b1selected === true ? this.dateCtrl.setValue(this.b1value) : this.dateCtrl.setValue(this.b2value);
  }

  setCurrentMonth() {
    this.currentMonth = this.b1selected === true ? this.datePipe.transform(this.b1value, 'MMM') : this.datePipe.transform(this.b2value, 'MMM');
    this.dateSelectionnee.emit(this.dateCtrl.value);
    //     console.log(this.dateCtrl.value);
  }

  onChange() {
    const dateChoisie = new Date(this.dateCtrl.value);
    const dateReference = this.b1selected === true ?
      Date.UTC(this.b1value.getFullYear(), this.b1value.getMonth(), this.b1value.getDate()) :
      Date.UTC(this.b2value.getFullYear(), this.b2value.getMonth(), this.b2value.getDate());

    const nbJoursEcart = Math.floor(
      (Date.UTC(dateChoisie.getFullYear(), dateChoisie.getMonth(), dateChoisie.getDate()) -
        dateReference)
      / (1000 * 60 * 60 * 24)
    );

    this.setDates(nbJoursEcart);
    this.setCurrentMonth();
  }

}
