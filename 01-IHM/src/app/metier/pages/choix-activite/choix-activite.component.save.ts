import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppConfig } from '../../../config/app-config';
import { HeureSport, La } from '../../modele';

import { LaService } from '../../services';
import { AuthenticationService } from '../../../applicatif';


@Component({
  selector: 'app-choix-activite',
  templateUrl: './choix-activite.component.html',
  styleUrls: ['./choix-activite.component.css']
})
export class ChoixActiviteComponent implements OnInit {
  @Input() dateSelectionnee: Date;
  @Output() activiteSelectionnee = new EventEmitter<Date>();

  natationSelected = 0;
  veloSelected = 0;
  capSelected = 0;

  nom: string;
  prenom: string;

  activitesN: HeureSport[] = [];
  activitesV: HeureSport[] = [];
  activitesC: HeureSport[] = [];


  // chaque entrée du tableau correspond à un jeu des 3 activités N, V, C
  // si pour un jour donné, il y a deux mêmes activités, alors le tableau comprend 2 entrées
  // activites: Map<string, HeureSport>[] = [];

  // chaque entrée de la Map correspond à une activité N, V, C
  // la Map obtenue contient tous les horaires possibles de cette activité
  mapActivites: Map<string, Map<string, HeureSport>>;

  constructor(
    private appConfig: AppConfig,
    private authenticationService: AuthenticationService,
    private laService: LaService
  ) { }


  ngOnInit(): void {
    this.nom = this.authenticationService.currentUserValue.lastName;
    this.prenom = this.authenticationService.currentUserValue.firstName;
  }

  /**
   * Initialise les heures disponibles pour les entrainements 
   * @param jour permet de récupérer a configuration des heures d'entrainements pour le jour considiéré
   */
  definirHeuresActivitesFigees(jour: Date) {
    this.reset();
    this.dateSelectionnee = jour;
    // console.log('[%s]', (new Date()).toISOString(), this.dateSelectionnee);
    // Récupération des horaires pour le jour sélectionné
    const horairesActivites = this.appConfig.mapDaysSports.get(jour.getDay());
    if (horairesActivites) {
      horairesActivites.forEach(a => {
        switch (a.activite) {
          case 'N':
            this.activitesN.push(a);
            break;
          case 'V':
            this.activitesV.push(a);
            break;
          case 'C':
            this.activitesC.push(a);
            break;
        }
      });
    } else {
      this.activitesN.push(new HeureSport('N', '', false));
      this.activitesV.push(new HeureSport('V', '', false));
      this.activitesC.push(new HeureSport('C', '', false));
    }

  }




  laPaslaNat(i) {
    this.natationSelected = i % 4;
    this.activitesN.forEach(a => a.choisi = i);
    // console.log(i, i % 4);
    this.enregistrerLa('N', i % 4);
  }

  laPaslaVelo(i) {
    this.veloSelected = i % 4;
    const activitV = this.determinerActiviteNonAmbigue('V', this.veloSelected);
    this.enregistrerLa2(activitV);
  }

  laPaslaCap(i) {
    this.capSelected = i % 4;
    this.activitesC.forEach(a => a.choisi = i);
    this.enregistrerLa('C', i % 4);
  }

  determinerActiviteNonAmbigue(d, choisi): HeureSport {
    let activite: HeureSport;
    switch (d) {
      case 'N':
        const activiteN = this.activitesN.filter(a => a.heure !== null);
        if (activiteN.length === 1) { activiteN[0].choisi = choisi; activite = activiteN[0]; }
        else { activite = new HeureSport('N', '', false, choisi); }
        break;
      case 'V':
        const activiteV = this.activitesV.filter(a => a.heure !== null);
        if (activiteV.length === 1) { activiteV[0].choisi = choisi; activite = activiteV[0]; }
        else { activite = new HeureSport('V', '', false, choisi); }
        break;
      case 'C':
        const activiteC = this.activitesC.filter(a => a.heure !== null);
        if (activiteC.length === 1) { activiteC[0].choisi = choisi; activite = activiteC[0]; }
        else { activite = new HeureSport('C', '', false, choisi); }
        break;
    }
    return activite;
  }


  reset() {
    this.activitesN = [];
    this.activitesV = [];
    this.activitesC = [];
    this.natationSelected = 0;
    this.veloSelected = 0;
    this.capSelected = 0;
  }


  /**
   * Permet de positionner les activités enregistrées au regard de celles figées
   * Se positionne vis à vis de celle figée
   * Ajoute une nouvelle entrée sinon
   * @param mesActivites : activités enregistrées
   */
  positionnerActivitesDejaSelectionnees(mesActivites: HeureSport[]) {

    // Parcours toutes les créneaux enregistrés
    mesActivites.forEach(a => {
      switch (a.activite) {
        case 'N':
          // Positionne la couleur selon le meilleur choix (eg : vert & rouge ==> vert)
          this.natationSelected = this.natationSelected === 0 ? a.choisi : Math.min(this.natationSelected, a.choisi);
          // Correspond à un créneau figé
          const activiteN = this.activitesN.filter(aN => aN.heure === a.heure);
          if (activiteN.length > 0) { activiteN[0].choisi = a.choisi; }
          // ajoute une entrée dans la liste des activités
          else { a.ordreAffichage = this.activitesN.length; this.activitesN.push(a); }
          break;
        case 'V':
          this.veloSelected = this.veloSelected === 0 ? a.choisi : Math.min(this.veloSelected, a.choisi);
          const activiteV = this.activitesV.filter(aV => aV.heure === a.heure);
          if (activiteV.length > 0) { activiteV[0].choisi = a.choisi; }
          else { a.ordreAffichage = this.activitesV.length; this.activitesV.push(a); }
          break;
        case 'C':
          this.capSelected = this.capSelected === 0 ? a.choisi : Math.min(this.capSelected, a.choisi);
          const activiteC = this.activitesC.filter(aC => aC.heure === a.heure);
          if (activiteC.length > 0) { activiteC[0].choisi = a.choisi; }
          else { a.ordreAffichage = this.activitesC.length; this.activitesC.push(a); }
          break;
      }
    });


    // Mise à niveau du contenu des tableaux de dimensions inférieurs
    const maxActivites = Math.max(this.activitesN.length, this.activitesV.length, this.activitesC.length);
    // console.log(maxActivites);
    for (let i = 0; i < maxActivites; i++) {
      if (this.activitesN.length < i + 1) { this.activitesN.push(new HeureSport('N', '', false, 0, i)); }
      if (this.activitesV.length < i + 1) { this.activitesV.push(new HeureSport('V', '', false, 0, i)); }
      if (this.activitesC.length < i + 1) { this.activitesC.push(new HeureSport('C', '', false, 0, i)); }
    }

  }

  choixHorairePiscine(e) {
    console.log(e.target.value);
    // activite.choisi 
  }

  choixHoraireVelo(e, choisi, ordreAffichage) {
    console.log(e.target.value, choisi, ordreAffichage);
    console.log(this.activitesV);
    this.activitesV[ordreAffichage].choisi = choisi === 0 ? 1 : choisi;
    this.laPaslaVelo(this.activitesV[ordreAffichage]);
    // console.log(e);
  }
  choixHoraireCAP(e) {
    // console.log(e);
  }

  enregistrerLa(quoi, i) {
    // console.log('[%s]', (new Date()).toISOString(), this.nom, this.prenom, quoi, this.dateSelectionnee, i);
    const la: La = new La();
    la.qui = this.nom + ' ' + this.prenom;
    la.quoi = quoi;
    la.la = i;
    const result = this.retrouverHeureSelectionnee(quoi);
    la.quandJ = result.dateJour;
    la.quandH = result.dateHeure;

    this.laService.postLa(la).subscribe(resp => {
      console.log('La enregistré : ', resp);
      this.activiteSelectionnee.emit(this.dateSelectionnee);
    });
  }


  enregistrerLa2(activite: HeureSport) {
    // console.log('[%s]', (new Date()).toISOString(), this.nom, this.prenom, quoi, this.dateSelectionnee, i);
    const la: La = new La();
    let dateHeure = '';

    la.qui = this.nom + ' ' + this.prenom;
    la.quoi = activite.activite;
    la.la = activite.choisi;

    const dateJour = new Date(this.dateSelectionnee);
    dateJour.setSeconds(parseInt('00', 10));
    if (activite.heure) {
      // console.log('***** case 1 - HEURE', activite.heure);
      dateJour.setHours(parseInt(activite.heure.substr(0, 2), 10));
      dateJour.setMinutes(parseInt(activite.heure.substr(3, 2), 10));
      dateHeure = activite.heure.substr(0, 2) + ':' + activite.heure.substr(3, 2);
    } else {
      // console.log('***** case 1 - PAS HEURE', activite.heure);
      dateJour.setHours(parseInt('02', 10)); // Sinon ca positionne la veille
      dateJour.setMinutes(parseInt('00', 10));
      dateHeure = '';
    }
    la.quandH = dateHeure;
    la.quandJ = dateJour;

    this.laService.postLa(la).subscribe(resp => {
      console.log('La enregistré : ', resp);
      this.activiteSelectionnee.emit(this.dateSelectionnee);
    });
  }

  retrouverHeureSelectionnee(quoi): { dateJour: Date, dateHeure: string } {
    const dateJour = new Date(this.dateSelectionnee);
    let dateHeure = '';

    const heuresActivitesPossibles = quoi === 'N' ?
      this.activitesN.filter(a => a.activite === quoi) : quoi === 'V' ?
        this.activitesV.filter(a => a.activite === quoi) :
        this.activitesC.filter(a => a.activite === quoi);
    switch (heuresActivitesPossibles.length) {
      case 0:
        // console.log('***** case 0');
        console.error('Il doit y avoir au moins une ligne');
        break;
      case 1:
        dateJour.setSeconds(parseInt('00', 10));
        if (heuresActivitesPossibles[0].heure) {
          // console.log('***** case 1 - HEURE', heuresActivitesPossibles[0].heure);
          dateJour.setHours(parseInt(heuresActivitesPossibles[0].heure.substr(0, 2), 10));
          dateJour.setMinutes(parseInt(heuresActivitesPossibles[0].heure.substr(3, 2), 10));
          dateHeure = heuresActivitesPossibles[0].heure.substr(0, 2) + ':' + heuresActivitesPossibles[0].heure.substr(3, 2);
        } else {
          // console.log('***** case 1 - PAS HEURE', heuresActivitesPossibles[0].heure);
          dateJour.setHours(parseInt('02', 10)); // Sinon ca positionne la veille
          dateJour.setMinutes(parseInt('00', 10));
          dateHeure = '';
        }

        // console.log('heuresActivitesPossibles', heuresActivitesPossibles[0], dateJour, dateHeure);
        break;
      default:
        console.log('***** case default');
        dateJour.setHours(parseInt('02', 10)); // Sinon ca positionne la veille
        dateJour.setMinutes(parseInt('00', 10));
        dateHeure = '';
        break;

    }
    return { dateJour, dateHeure };
  }

}

