import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppConfig } from '../../../config/app-config';
import { HeureSport, La } from '../../modele';

import { LaService } from '../../services';
import { AuthenticationService } from '../../../applicatif';


@Component({
  selector: 'app-choix-activite',
  templateUrl: './choix-activite.component.html',
  styleUrls: ['./choix-activite.component.css']
})
export class ChoixActiviteComponent implements OnInit {
  @Input() dateSelectionnee: Date;
  @Output() activiteSelectionnee = new EventEmitter<HeureSport>();

  idAff = 0; // Générateur d'Id pour l'affichage


  natationSelected = 0;
  veloSelected = 0;
  capSelected = 0;

  activitesN: HeureSport[] = [];
  activitesV: HeureSport[] = [];
  activitesC: HeureSport[] = [];

  // chaque entrée du tableau correspond à un jeu des 3 activités N, V, C
  // si pour un jour donné, il y a deux mêmes activités, alors le tableau comprend 2 entrées
  // activites: Map<string, HeureSport>[] = [];

  // chaque entrée de la Map correspond à une activité N, V, C
  // la Map obtenue contient tous les horaires possibles de cette activité
  mapActivites: Map<string, Map<string, HeureSport>>;

  constructor(
    private appConfig: AppConfig,
    private authenticationService: AuthenticationService,
    private laService: LaService
  ) { }


  ngOnInit(): void {
    this.reset();

  }

  reset() {
    this.idAff = 0;
    this.activitesN = [];
    this.activitesV = [];
    this.activitesC = [];
    this.natationSelected = 0;
    this.veloSelected = 0;
    this.capSelected = 0;
  }

  /**
   * Incrément de l'id interne
   */
  getNextIdAff(): string {
    this.idAff = this.idAff + 1;
    return this.appConfig.IDAFF + this.idAff;
  }

  afficherActivites(jour: Date, las: La[]): HeureSport[] {
    this.reset();
    this.definirHeuresActivitesFigees(jour);
    const mesActivites = this.definirMesActivites(las);
    this.ajouterMesActivitesDejaSelectionnees(mesActivites);
    this.definirHeuresActivitesNonPourvues();

    return this.activitesN.concat(this.activitesV).concat(this.activitesC).filter(a => a.choisi > 0);
  }

  /**
   * Initialise les heures disponibles pour les entrainements 
   * Est appelé à chaque changement de jour
   * @param jour permet de récupérer a configuration des heures d'entrainements pour le jour considiéré
   */
  definirHeuresActivitesFigees(jour: Date) {
    this.dateSelectionnee = jour;
    // Récupération des horaires pour le jour sélectionné
    const horairesActivites = this.appConfig.mapDaysSports.get(jour.getDay());
    if (horairesActivites) {
      horairesActivites.forEach(a => {
        switch (a.activite) {
          case 'N': this.activitesN.push(a.clone()); break;
          case 'V': this.activitesV.push(a.clone()); break;
          case 'C': this.activitesC.push(a.clone()); break;
        }
        // Positionne l'incrément interne au bon niveau
        this.idAff = Math.max(this.idAff, parseInt(a.idAff.substr(5, a.idAff.length), 10));
      });
    } 
  }

  definirHeuresActivitesNonPourvues() {
    // Mise à niveau du contenu des tableaux de dimensions inférieurs
    // Si aucune activité n'est déjà positionnée ==> max = 1
    const maxActivites = Math.max(this.activitesN.length, this.activitesV.length, this.activitesC.length, 1);
    for (let i = 0; i < maxActivites; i++) {
      if (this.activitesN.length < i + 1) { this.activitesN.push(new HeureSport(this.getNextIdAff(), 'N', '', false, 0, i)); }
      if (this.activitesV.length < i + 1) { this.activitesV.push(new HeureSport(this.getNextIdAff(), 'V', '', false, 0, i)); }
      if (this.activitesC.length < i + 1) { this.activitesC.push(new HeureSport(this.getNextIdAff(), 'C', '', false, 0, i)); }
    }
  }

  /**
   * Transforme la liste des las récupérées de la base en HeureSpor[] pour l'affchage
   * @param las
   */
  definirMesActivites(las: La[]): HeureSport[] {
    const mesActivites = [];
    // seuls les las du user ont la valeur creePar renseigné
    const mesLas: La[] = las.filter(l => l.creePar !== null);
    mesLas.forEach(l => {
      const activite = new HeureSport(l.id, l.quoi, l.quandH, l.fige, l.la, null, l.id);
      mesActivites.push(activite);
    });
    return mesActivites;

  }

  /**
   * Permet de positionner les activités enregistrées au regard de celles figées
   * Se positionne vis à vis de celle figée
   * Ajoute une nouvelle entrée sinon
   * @param mesActivites : activités enregistrées
   */
  ajouterMesActivitesDejaSelectionnees(mesActivites: HeureSport[]) {

    // Parcours toutes les créneaux enregistrés
    mesActivites.forEach(a => {
      switch (a.activite) {
        case 'N':
          // Correspond à un créneau figé
          const activiteN = this.activitesN.filter(aN => aN.heure === a.heure && aN.horaireFige === a.horaireFige);
          // Si un créneau figé correspond à une activité sélectionnée, il faut mettre à jour ce créneau avec les données personalisées
          if (activiteN.length > 0) { activiteN[0].choisi = a.choisi; activiteN[0].id = a.id; activiteN[0].idAff = a.id; }
          // sinon ajoute une entrée supplémentaire dans la liste des activités
          else { a.ordreAffichage = this.activitesN.length; this.activitesN.push(a); }
          break;
        case 'V':
          const activiteV = this.activitesV.filter(aV => aV.heure === a.heure && aV.horaireFige === a.horaireFige);
          if (activiteV.length > 0) { activiteV[0].choisi = a.choisi; activiteV[0].id = a.id; activiteV[0].idAff = a.id; }
          else { a.ordreAffichage = this.activitesV.length; this.activitesV.push(a); }
          break;
        case 'C':
          const activiteC = this.activitesC.filter(aC => aC.heure === a.heure && aC.horaireFige === a.horaireFige);
          if (activiteC.length > 0) { activiteC[0].choisi = a.choisi; activiteC[0].id = a.id; activiteC[0].idAff = a.id; }
          else { a.ordreAffichage = this.activitesC.length; this.activitesC.push(a); }
          break;
      }
      // Positionne la couleur selon le meilleur choix (eg : vert & rouge ==> vert)
      this.definirActiviteSelected(a);
    });
  }

  definirActiviteSelected(a: HeureSport) {
    switch (a.activite) {
      case 'N': this.natationSelected = Math.min(...this.activitesN.filter(hs => hs.choisi > 0).map(hs => hs.choisi)); break;
      case 'V': this.veloSelected = Math.min(...this.activitesV.filter(hs => hs.choisi > 0).map(hs => hs.choisi)); break;
      case 'C': this.capSelected = Math.min(...this.activitesC.filter(hs => hs.choisi > 0).map(hs => hs.choisi)); break;
    }
  }


  laPasla(a: HeureSport) {
    this.definirActiviteSelected(a);
    this.enregistrerLa(a);
  }

  laPaslaActivite(d) {
    let activites: HeureSport[];
    switch (d) {
      case 'N': activites = this.activitesN; break;
      case 'V': activites = this.activitesV; break;
      case 'C': activites = this.activitesC; break;
    }
    // S'il n'y a qu'un élément dans la liste il n'y a pas d'ambiguité
    if (activites.length === 1) {
      activites[0].choisi = (activites[0].choisi + 1) % 4;
      this.laPasla(activites[0]);
    }
    // Si un seul élément a un statut positionné :
    else if (activites.filter(a => a.choisi > 0).length === 1) {
      const activite = activites.filter(a => a.choisi > 0)[0];
      activite.choisi = (activite.choisi + 1) % 4;
      this.laPasla(activite);
    }
    // S'il y a plusieurs lignes avec horaires choisis
    else if (activites.filter(a => a.choisi !== 0).filter(a => a.heure !== '').length > 0) {
      const activite = activites.filter(a => a.choisi !== 0).filter(a => a.heure !== '')[0];
      console.warn('BLINK à CREER');

    }
    // S'il y a un plusierus créneaux mais un seul horaire est positionné :
    else if (activites.filter(a => a.choisi === 0).filter(a => a.heure !== '').length === 1) {
      const activite = activites.filter(a => a.choisi === 0).filter(a => a.heure !== '')[0];
      activite.choisi = (activite.choisi + 1) % 4;
      this.laPasla(activite);
    }
    // S'il y a plusieurs lignes sans horaires, on prend le premier élément de la liste
    else if (activites.filter(a => a.choisi === 0).filter(a => a.heure === '').length > 0) {
      const activite = activites.filter(a => a.choisi === 0).filter(a => a.heure === '')[0];
      activite.choisi = (activite.choisi + 1) % 4;
      this.laPasla(activite);
    }
    // S'il y a plusieurs lignes avec horaires non choisis, on ajoute un élément dans la liste
    else if (activites.filter(a => a.choisi === 0).filter(a => a.heure !== '').length > 0) {
      const activite = activites.filter(a => a.choisi === 0).filter(a => a.heure !== '')[0];
      const newActivite = new HeureSport(this.getNextIdAff(), activite.activite, '', false, 1, activites.length);
      const mesNewActivites: HeureSport[] = [];
      mesNewActivites.push(newActivite);
      this.ajouterMesActivitesDejaSelectionnees(mesNewActivites);
      this.laPasla(newActivite);
    }

  }

  /**
   * Choisir un horaire alors que l'activité est vierge correspond à la choisir
   * @param e 
   * @param a 
   */
  laPaslaHoraireModifiable(e, a: HeureSport) {
    let activite: HeureSport[];
    a.heure = e.target.value;

    switch (a.activite) {
      case 'N': activite = this.activitesN.filter(aN => aN.heure === a.heure); break;
      case 'V': activite = this.activitesV.filter(aV => aV.heure === a.heure); break;
      case 'C': activite = this.activitesC.filter(aC => aC.heure === a.heure); break;
    }
    activite[0].choisi = a.heure === '' ? 0 : 1;
    this.laPasla(activite[0]);

  }


  /**
   * Cliquer sur un horaire renseigné permet de changer son statut
   * @param e 
   * @param a : activite qui obtient le focus
   */
  laPaslaHoraireFige(e, a: HeureSport, choisi) {
    let activite: HeureSport[];

    // cliquer sur une heure vide ne change rien
    // if (a.heure !== '') {
    if (e.target.readOnly) {
      a.choisi = choisi % 4;
      switch (a.activite) {
        case 'N': activite = this.activitesN.filter(aN => aN.heure === a.heure); break;
        case 'V': activite = this.activitesV.filter(aV => aV.heure === a.heure); break;
        case 'C': activite = this.activitesC.filter(aC => aC.heure === a.heure); break;
      }
      // Si le créneau est trouvé et que l'activité n'est pas sélectionnée, on la considère comme choisie
      if (activite.length > 0) { activite[0].choisi = a.choisi; this.laPasla(activite[0]); }
    }
  }

  changerStatutSeance(activiteAffichee) {
    let activite: HeureSport[];
    switch (activiteAffichee.quoi) {
      case 'N': activite = this.activitesN.filter(a => a.idAff === activiteAffichee.id); break;
      case 'V': activite = this.activitesV.filter(a => a.idAff === activiteAffichee.id); break;
      case 'C': activite = this.activitesC.filter(a => a.idAff === activiteAffichee.id); break;
    }
    if (activite.length > 0) {
      activite[0].choisi = (activite[0].choisi + 1) % 4;
      this.laPasla(activite[0]);
    }

  }


  /**
   * 
   * @param activite : activité à enregistrer
   * @return l'id créé
   */
  enregistrerLa(activite: HeureSport) {
    const la: La = new La();
    let dateHeure = '';

    la.id = activite.id;
    la.qui = this.authenticationService.currentUserValue.pseudo;
    la.quoi = activite.activite;
    la.la = activite.choisi;
    la.fige = activite.horaireFige;

    const dateJour = new Date(this.dateSelectionnee);
    dateJour.setSeconds(parseInt('00', 10));
    if (activite.heure) {
      dateJour.setHours(parseInt(activite.heure.substr(0, 2), 10));
      dateJour.setMinutes(parseInt(activite.heure.substr(3, 2), 10));
      dateHeure = activite.heure.substr(0, 2) + ':' + activite.heure.substr(3, 2);
    } else {
      dateJour.setHours(parseInt('02', 10)); // Sinon ca positionne la veille
      dateJour.setMinutes(parseInt('00', 10));
      dateHeure = '';
    }
    la.quandH = dateHeure;
    la.quandJ = dateJour;

    this.activiteSelectionnee.emit(activite);

    /**
     * L'enregistrement en base est totalement désynchronisé de l'affichage
     * Un mmaping entre l'affichage et la base est dès lors maintenu
     */

    this.laService.postLa(la).subscribe(resp => {
      switch (activite.activite) {
        case 'N': this.activitesN[activite.ordreAffichage].id = resp.id; break;
        case 'V': this.activitesV[activite.ordreAffichage].id = resp.id; break;
        case 'C': this.activitesC[activite.ordreAffichage].id = resp.id; break;
      }

    });
  }


}

