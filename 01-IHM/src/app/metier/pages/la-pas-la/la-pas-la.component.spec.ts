import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaPasLaComponent } from './la-pas-la.component';

describe('LaPasLaComponent', () => {
  let component: LaPasLaComponent;
  let fixture: ComponentFixture<LaPasLaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaPasLaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaPasLaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
