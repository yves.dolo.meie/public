import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';


import { La } from '../../modele';


class LaAffiche extends La {
  color = '';
  icon = '';
  backgroundColor = '';

  constructor(la: La) {
    super();
    this.qui = la.qui; this.quoi = la.quoi; this.quandJ = la.quandJ; this.quandH = la.quandH; this.la = la.la; this.id = la.id;
  }
}

const ICONS: string[] = ['', 'check_circle_outline', 'help_outline', 'highlight_off'];
const COLORS: string[] = ['', 'green', 'orange', 'red'];
const CELL_COLORS: string[] = ['', '#b5ff0059', '#ffb0006b', '#f7303059'];


@Component({
  selector: 'app-la-pas-la',
  templateUrl: './la-pas-la.component.html',
  styleUrls: ['./la-pas-la.component.css']
})
export class LaPasLaComponent implements OnInit, AfterViewInit {
  @Output() seanceSelectionnee = new EventEmitter<string>();
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  titreColonnes: string[] = ['Qui', 'Quoi', 'Quand', 'laPlusla'];

  lasAffiches: LaAffiche[] = [];


  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => {
      this.sortData();
    });
  }

  sortData() {
    const data = this.lasAffiches.slice();
    if (!this.sort.active || this.sort.direction === '') {
      this.lasAffiches = data;
      return;
    }

    this.lasAffiches = data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'Qui': return compare(a.qui, b.qui, isAsc);
        case 'Quoi': return compare(a.quoi, b.quoi, isAsc);
        case 'Quand': return compare(a.quandH, b.quandH, isAsc);
        case 'laPlusla': return compare(a.la, b.la, isAsc);
        default: return 0;
      }
    });
  }

  afficherQuiEstla(las: La[]) {
    this.lasAffiches = [];
    // prépare l'affichage des icones
    las.filter(l => l.la !== 0).forEach(l => {
      const laAffiche = new LaAffiche(l);
      laAffiche.color = COLORS[l.la];
      laAffiche.icon = ICONS[l.la];
      laAffiche.backgroundColor = l.creePar !== null ? CELL_COLORS[l.la] : '';
      this.lasAffiches.push(laAffiche);
    });
  }

  onTableScroll(e) {
    const tableViewHeight = e.target.offsetHeight; // viewport: ~500px
    const tableScrollHeight = e.target.scrollHeight; // length of all table
    const scrollLocation = e.target.scrollTop; // how far user scrolled

    // If the user has scrolled within 200px of the bottom, add more data
    const buffer = 200;
    const limit = tableScrollHeight - tableViewHeight - buffer;
    if (scrollLocation > limit) {
      // this.dataSource = this.dataSource.concat(ELEMENT_DATA);
    }
  }


  laPaslaHoraire(activite) {
    this.seanceSelectionnee.emit(activite);
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
