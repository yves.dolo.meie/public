import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as appActions from '../../../applicatif/communs/store/app.actions';
import * as fromRoot from '../../../applicatif/communs/store';

import { ChoixActiviteComponent, LaPasLaComponent } from '..';
import { DatePipe } from '@angular/common';
import { AlertService, AuthenticationService } from '../../../applicatif';
import { LaService } from '../../services';
import { HeureSport, La } from '../../modele';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  @ViewChild(ChoixActiviteComponent, { static: true }) choixActiviteComponent: ChoixActiviteComponent;
  @ViewChild(LaPasLaComponent, { static: true }) laPasLaComponent: LaPasLaComponent;

  DateInit: Date = new Date();
  dateSelectionnee: Date = new Date();
  lasBase: La[] = [];
  lasAffiches: HeureSport[] = [];

  isLoading = true;

  constructor(
    private authenticationService: AuthenticationService,
    private datePipe: DatePipe,
    private laService: LaService,
    private alertService: AlertService,
    private store: Store<fromRoot.State>) { }

  ngOnInit(): void {
    this.alertService.getAlert().subscribe(alert => this.traiterAlerte(alert));
  }

  onDateSelectionne(e) {
    this.rechercherQuiEstla(e);
  }

  onActiviteSelectionnee(activite: HeureSport) {
    this.synchroniserBaseEtAffichage(activite);
  }

  rechercherQuiEstla(d: Date) {
    this.alertService.clear(); // obligé pour pouvoir traiter l'erreur
    this.isLoading = true;
    this.laService.getLasFromDate(this.datePipe.transform(d, 'yyyyMMdd'))
      .subscribe(las => {
        this.isLoading = false;
        this.lasBase = las;
        this.laPasLaComponent.afficherQuiEstla(this.lasBase);
        this.lasAffiches = this.choixActiviteComponent.afficherActivites(d, this.lasBase);
      }, error => this.isLoading = false);
  }

  synchroniserBaseEtAffichage(activite: HeureSport) {
    const las = this.lasBase.filter(l => l.id === activite.idAff);
    if (las.length === 1) {
      las[0].la = activite.choisi;
      las[0].quandH = activite.heure !== '' ? activite.heure.substr(0, 2) + ':' + activite.heure.substr(3, 2) : '';
    } else {
      const la = new La();
      la.id = activite.idAff;
      la.qui = this.authenticationService.currentUserValue.pseudo;
      la.quoi = activite.activite;
      la.la = activite.choisi;
      la.quandH = activite.heure !== '' ? activite.heure.substr(0, 2) + ':' + activite.heure.substr(3, 2) : '';
      this.lasBase.push(la);
    }

    this.laPasLaComponent.afficherQuiEstla(this.lasBase);
  }

  onSeanceSelectionnee(activite) {
    this.choixActiviteComponent.changerStatutSeance(activite);
  }


  swipeLeft() {
    this.store.dispatch(new appActions.SwipeLeft());
  }

  swipeRight() {
    this.store.dispatch(new appActions.SwipeRight());
  }

  traiterAlerte(alert) {
    this.isLoading = false;
  }

}
