
export class La {
    id: String;
    qui: String;
    quandJ: Date; // Jour
    quandH: string; // au format HH:mm
    quoi: String;
    fige: Boolean;
    la: Number;
    creeLe: Date;
    modifieLe: Date;
    creePar: String;
    key: String;
    constructor(la: La) {
        this.id = la.id;
        this.creeLe = la.creeLe;
        this.modifieLe = la.modifieLe;
        this.creePar = la.creePar;
        this.qui = la.qui;
        this.la = la.la;
        this.quandJ = la.quandJ;
        this.quandH = la.quandH;
        this.quoi = la.quoi;
        this.fige = la.fige;
        this.key = la.key;
    }

}


export class LaReponse {
    response: String;

    constructor(response: String) {
        this.response = response;
    }
}


