import mongoose from "mongoose";

const LaSchema = new mongoose.Schema ({
    qui: String,
    quandJ: Date,
    quandH: String, // au format HH:mm
    quoi: String,
    fige: Boolean, // Permet de distinguer les horaires figés des autres
    la: Number,
    creeLe: Date,
    modifieLe: Date,
    creePar: String,
    key: String,
});

const LaReponseSchema = new mongoose.Schema ({
    response: String
});

export const LaEntity = mongoose.model("presents", LaSchema);
export const LaResponseEntity = mongoose.model("LaResponse", LaReponseSchema);
