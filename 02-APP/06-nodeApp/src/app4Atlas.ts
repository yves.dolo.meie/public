import express, { Application } from 'express';
import { Controller } from './main.controller';
import { MONGO_ATLAS_URL, MONGO_GCP_URL } from './constants/lapaslaApi.constants';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';

class App {
  public app: Application;
  public surveyController: Controller

  constructor() {
    this.app = express();
    this.setConfig();
    this.setMongoConfig();

    //Creating and assigning a new instance of our controller
    this.surveyController = new Controller(this.app);
  }

  private setConfig() {
    //Allows us to receive requests with data in json format
    this.app.use(bodyParser.json({ limit: '50mb' }));

    //Allows us to receive requests with data in x-www-form-urlencoded format
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended:true}));

    //Enables cors   
    this.app.use(cors());
  }

    //Connecting to our MongoDB database
    private setMongoConfig() {
        mongoose.Promise = global.Promise;
        mongoose.connect(MONGO_ATLAS_URL, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          autoIndex: false, // Don't build indexes
          // The option `reconnectTries` is incompatible with the unified topology, please read more by visiting http://bit.ly/2D8WfT6 ==> reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
          // The option `reconnectInterval` is incompatible with the unified topology, please read more by visiting http://bit.ly/2D8WfT6 ==> reconnectInterval: 500, // Reconnect every 500ms
          poolSize: 10, // Maintain up to 10 socket connections
          // If not connected, return errors immediately rather than waiting for reconnect
          bufferMaxEntries: 0,
          connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
          socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
        }).catch(err => console.log('[' + Date.now() + '] : Connection initiale impossible'));
        mongoose.connection.on('error', err => console.log('[' + Date.now() + '] : Connection perdue'));
      }

}

export default new App().app;