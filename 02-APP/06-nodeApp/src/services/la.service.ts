import { Request, Response } from "express";
import { WELCOME_MESSAGE } from '../constants/lapaslaApi.constants';
import { MongooseDocument } from 'mongoose';
import { LaEntity, LaResponseEntity } from "../model/la.mongoose.model";
import { La, LaReponse } from "../model/la.model";


export class SurveyService {


  public getHealthCheckProbe(req: Request, res: Response) {
    return res.status(200).send();
  }

  //Getting data from the db
  public getAllLas(req: Request, res: Response) {

    console.log('[%s] Récupération de tous les las', new Date());
    console.log('[%s] paramètres transmis', new Date(), req.query);


    LaEntity.find({}, (error: Error, las: MongooseDocument) => {
      if (error) {
        res.send(error);
      }
      res.json(las);
    });
  }

  //Getting data from the db
  public getLasFromDate(req: Request, res: Response) {

    console.log('[%s] Récupération de tous les las', new Date());
    console.log('[%s] paramètres transmis', new Date(), req.query.d);
    const dateInf = new Date(req.query.d.substring(0, 4), req.query.d.substring(4, 6) - 1, req.query.d.substring(6, 8)); // -1 car Janvier = 0
    const dateSup = new Date(dateInf);
    dateSup.setDate(dateSup.getDate() + 1);
    //const dateSup = new Date().setDate(dateInf.getDate());

    console.log('[%s] date obtenue', new Date(), dateInf, dateSup);

    // Mongo query = {"quand" : { $lt: new ISODate("2020-06-21"), $gte : new ISODate("2020-06-20")}};
    // const query = {"quand" : { $lt: new Date("2020-06-24"), $gte : new Date("2020-06-23")}};
    // const query = {"quand" : { $lt: "2020-06-24", $gte : "2020-06-23"}};
    const query = { "quandJ": { $gte: dateInf, $lt: dateSup } };
    console.log('[%s] query', new Date(), query);

    LaEntity.find(query, (error: Error, las: MongooseDocument) => {
      if (error) {
        res.send(error);
      }

      res.json(las);
      console.log('[%s] fin getLasFromDate', (new Date().toISOString()));
      // res.json(las);
    });
  }

  //Adding a new La or Update existing one

  public la(req: Request, res: Response) {

    const newLa = new La(req.body);
    console.log('\n***************\n[%s] Ajout de La', new Date(), newLa);

    if (newLa.key == null) {
      const uuidv4 = require('uuid/v4');
      newLa.key = uuidv4();
    }

    const newLaEntity = new LaEntity(newLa);
    if (newLa.id) {
      newLa.modifieLe = new Date();
      LaEntity.findByIdAndUpdate(
        newLa.id, newLa, (error: Error, la: any) => {
        if (error) {
          res.send(error);
          console.log(Date.now() + ' : Impossible de sauvegarder : ', newLa);
        } else {
          console.log('[%s] La ajouté', new Date(), la);
          res.json(la);
        }
      });


    } else {
      newLa.creeLe = new Date();
      newLaEntity.save((error: Error, la: MongooseDocument) => {
        if (error) {
          res.send(error);
          console.log(Date.now() + ' : Impossible de sauvegarder : ', newLa);
        } else {
          console.log('[%s] La ajouté', new Date(), la);
          res.json(la);
        }
      });

    }
  }


  public deleteLa(req: Request, res: Response) {
    const laID = req.params.id;
    LaEntity.findByIdAndDelete(laID, (error: Error, deleted: any) => {
      if (error) {
        res.send(error);
      }
      const message = deleted ? 'Deleted successfully' : 'La not found :(';
      res.send(message);
    });
  }


  public updateLa(req: Request, res: Response) {
    const laId = req.params.id;
    LaEntity.findByIdAndUpdate(
      laId,
      req.body,
      (error: Error, La: any) => {
        if (error) {
          res.send(error);
        }
        const message = La
          ? 'Updated successfully'
          : 'La not found :(';
        res.send(message);
      }
    );
  }

}
