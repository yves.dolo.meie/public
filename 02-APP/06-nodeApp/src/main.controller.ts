import { Application } from 'express';
import { SurveyService } from './services/la.service'

export class Controller {
    private surveyService: SurveyService;

    constructor(private app: Application) {
        this.surveyService = new SurveyService();
        this.routes();
    }

    public routes() {
        this.app.route('/').get(this.surveyService.getHealthCheckProbe);
        this.app.route("/presences").get(this.surveyService.getAllLas);
        this.app.route("/quiestla").get(this.surveyService.getLasFromDate);
        this.app.route("/jesuisla").post(this.surveyService.la);
        this.app.route("/jesuispasla").post(this.surveyService.la);
        this.app.route("/jesuisplusla").post(this.surveyService.la);
        
        /* Chaining our route.
         * Considering that both the delete and put routes have exactly the same endpoint, 
         * we can chain them as shown above. This way, we don’t have to declare the same route twice, one for each verb.
        */

/*
// Inutile dans un premier temps
        this.app
            .route("/sondage/:id")
            .delete(this.surveyService.deleteSondage)
            .put(this.surveyService.updateSondage);
*/
    }
}