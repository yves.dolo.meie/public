package ydo.lapaslaWApp.metier.service.impl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ydo.lapaslaWApp.applicatif.AppReponse;
import ydo.lapaslaWApp.applicatif.AppReponseAppUser;
import ydo.lapaslaWApp.applicatif.AppReponseAppUserToken;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.traitement.AccountManagementTraitement;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static ydo.lapaslaWApp.metier.controller.response.ErrorMessages.*;

@SpringBootTest
class AccountMgtServiceImplTest {

  @MockBean
  public AccountManagementTraitement accountManagementTraitement;

  @InjectMocks
  @Autowired
  private AccountMgtServiceImpl accountMgtService;

  String mailExistingUser = "plo@tri.exist";
  String mailNewUser = "plo@tri.new";
  String token = "123456-123456";
  String newPassword = "az123-AZ456%";
  AppUserToken appUserToken = new AppUserToken(null, mailExistingUser, token, new Date(), null);
  User existingUser = new User(null, mailExistingUser, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr" );
  AppUser existingAppUser = new AppUser(null, mailExistingUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailExistingUser, null, new Date(), null) ;
  User newUser = new User(null, mailNewUser, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr" );
  AppUser newAppUser = new AppUser(null, mailNewUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailNewUser, null, new Date(), null) ;
  AppUser savedNewAppUser = new AppUser("aeioy0123456789", mailNewUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailNewUser, null, new Date(), null) ;

  @Test
  void postRegister() {
    // Vérification login existant
    when(accountManagementTraitement.isUserExists(existingUser)).thenReturn(new AppReponseAppUser(existingAppUser));
    assertEquals(ERR20, accountMgtService.postRegister(existingUser));
    verify(accountManagementTraitement, times(1)).isUserExists(existingUser);

    // Vérification nouveau compte
    when(accountManagementTraitement.isUserExists(newUser)).thenReturn(new AppReponseAppUser(ERR100, 2, Class.class ));
    when(accountManagementTraitement.saveUser(newUser)).thenReturn(new AppReponseAppUser(savedNewAppUser));
    //when(accountManagementTraitement.notifMailUser(newUser, UseCase.REGISTER)).thenReturn(null);
    assertEquals(0, accountMgtService.postRegister(newUser));
    verify(accountManagementTraitement, times(1)).isUserExists(existingUser);

  }

  @Test
  void postNewPasswordTockenGenerator() {
    when(accountManagementTraitement.isUserExists(existingUser.getLogin())).thenReturn(new AppReponseAppUser(existingAppUser));
    assertEquals(0, accountMgtService.postNewPasswordTockenGenerator(existingUser.getLogin()));
  }

  @Test
  void postResetPassword() {


    // Cas token non existant
    when(accountManagementTraitement.isTokenExists(token)).thenReturn(new AppReponseAppUserToken(ERR112, 1, Class.class));
    assertEquals(ERR112, accountMgtService.postResetPassword(token, existingUser.getLogin()));

    // Cas token existant mais non valide
    when(accountManagementTraitement.isTokenExists(token)).thenReturn(new AppReponseAppUserToken(appUserToken));
    when(accountManagementTraitement.isTokenValid(appUserToken)).thenReturn(new AppReponseAppUserToken(ERR331, 2, Class.class));
    assertEquals(ERR331, accountMgtService.postResetPassword(token, existingUser.getLogin()));

    // Cas token existant, valide, mais utilisateur non existant
    when(accountManagementTraitement.isTokenExists(token)).thenReturn(new AppReponseAppUserToken(appUserToken));
    when(accountManagementTraitement.isTokenValid(appUserToken)).thenReturn(new AppReponseAppUserToken(appUserToken));
    when(accountManagementTraitement.isUserExists(appUserToken.getUsername())).thenReturn(new AppReponseAppUser(ERR100, 2, Class.class));
    assertEquals(ERR332, accountMgtService.postResetPassword(token, existingUser.getLogin()));

    // Cas token existant et valide
    when(accountManagementTraitement.isTokenExists(token)).thenReturn(new AppReponseAppUserToken(appUserToken));
    when(accountManagementTraitement.isTokenValid(appUserToken)).thenReturn(new AppReponseAppUserToken(appUserToken));
    when(accountManagementTraitement.isUserExists(appUserToken.getUsername())).thenReturn(new AppReponseAppUser(existingAppUser));
    // Je ne comprends pas la raison de modifier dans le cas ci-après les paramètres d'entrée. Les mécanismes en jeu sont les mêmes que précédemment
    when(accountManagementTraitement.changePassword(any(AppUser.class), any(String.class))).thenReturn(new AppReponseAppUser(savedNewAppUser));
    assertEquals(0, accountMgtService.postResetPassword(token, existingUser.getLogin()));
  }

}
