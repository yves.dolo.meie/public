package ydo.lapaslaWApp.metier.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ydo.lapaslaWApp.applicatif.AppReponse;
import ydo.lapaslaWApp.applicatif.AppReponseAppUser;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.service.AccountMgtService;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import ydo.lapaslaWApp.securite.security.repository.AppUserTockenRepository;
import ydo.lapaslaWApp.securite.security.repository.RefreshTokenRepository;

import java.nio.charset.Charset;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountMgtController.class)
class AccountMgtControllerTest {

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));


  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private AccountMgtService accountMgtService;

  @MockBean
  private RefreshTokenRepository refreshTokenRepository;
  @MockBean
  private AppUserRepository appUserRepository;
  @MockBean
  private AppUserTockenRepository appUserTockenRepository;

  @Test
  public void registerProcessOk() throws Exception {
    User user = new User(null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr" );
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String jsonUser=ow.writeValueAsString(user);
    when(accountMgtService.postRegister(user)).thenReturn((new AppReponseAppUser()).getErrCode());

    this.mockMvc.perform(post("/api/register", user)
      .contentType(APPLICATION_JSON_UTF8)
      .content(jsonUser))
      .andDo(print())
      .andExpect(status().isOk());
      //.andExpect(content().string(containsString("Hello, Mock")));
  }
}
