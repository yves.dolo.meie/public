package ydo.lapaslaWApp.metier.traitement;

import org.springframework.boot.test.mock.mockito.MockBean;
import ydo.lapaslaWApp.config.UseCase;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.traitement.mail.MailTraitement;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import ydo.lapaslaWApp.securite.security.repository.AppUserTockenRepository;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static ydo.lapaslaWApp.metier.controller.response.ErrorMessages.*;

@SpringBootTest
class AccountManagementTraitementTest {

  String mailExistingUser = "plo@tri.exist";
  String mailNewUser = "plo@tri.new";
  String token = "123456-123456";
  User existingUser = new User(null, mailExistingUser, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr" );
  AppUser existingAppUser = new AppUser(null, mailExistingUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailExistingUser, null, new Date(), null) ;
  User newUser = new User(null, mailNewUser, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", "plo@tri.fr" );
  AppUser newAppUser = new AppUser(null, mailNewUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailNewUser, null, new Date(), null) ;
  AppUser savedNewAppUser = new AppUser("aeioy0123456789", mailNewUser, null, null, "plo@tri.fr", "plo@tri.fr", "plo@tri.fr", mailNewUser, null, new Date(), null) ;

  @MockBean
  private AppUserRepository appUserRepository;
  @MockBean
  private AppUserTockenRepository appUserTockenRepository;
  @MockBean
  private MailTraitement mailTraitement;




  @Test
  void isUserExists() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);

    // Cas d'un utilisateur existant
    when(appUserRepository.findByUsername(existingUser.getLogin())).thenReturn(existingAppUser);
    assertEquals(existingUser.getLogin(),(accountManagementTraitement.isUserExists(existingUser).getObjetReponse()).getEmail());

    // Cas d'un utilisateur non existant
    when(appUserRepository.findByUsername(newUser.getLogin())).thenReturn(null);
    assertEquals(ERR100, accountManagementTraitement.isUserExists(newUser).getErrCode());
  }

  @Test
  void saveUser() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);

    // Vérification du mapping AppUser.username = User.login + des codes retour uniquement
    //  Cf https://mkyong.com/spring-boot/spring-mockito-unable-to-mock-save-method/
    // Cas sauvegarde succès
    when(appUserRepository.save(any(AppUser.class))).thenReturn(savedNewAppUser);
    assertEquals(newUser.getLogin(),(accountManagementTraitement.saveUser(newUser).getObjetReponse()).getEmail());

    // Cas sauvegarde échec
    when(appUserRepository.save(any(AppUser.class))).thenReturn(null);
    assertEquals(ERR10, accountManagementTraitement.saveUser(newUser).getErrCode());

  }

  @Test
  void changePassword() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);


    // Cas sauvegarde succès
    when(appUserRepository.save(any(AppUser.class))).thenReturn(existingAppUser);
    assertEquals(existingUser.getLogin(),(accountManagementTraitement.changePassword(existingAppUser, "aZ.123456").getObjetReponse()).getEmail());

    // Cas sauvegarde échec
    when(appUserRepository.save(any(AppUser.class))).thenReturn(null);
    assertEquals(ERR333, accountManagementTraitement.changePassword(existingAppUser, "aZ.123456").getErrCode());
  }

  @Test
  void revokeAllTokens() {
  }

  @Test
  void notifMailUser() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);

    when(appUserTockenRepository.save(any(AppUserToken.class))).thenReturn(new AppUserToken());
    when(mailTraitement.sendMailToUser(newUser.getLogin(), "Objet", "contenu")).thenReturn(0);
    assertEquals(0, accountManagementTraitement.notifMailUser(newAppUser, UseCase.REGISTER));
    assertEquals(0, accountManagementTraitement.notifMailUser(existingAppUser, UseCase.FORGOT_PWD));
    assertEquals(0, accountManagementTraitement.notifMailUser(existingAppUser, UseCase.PWD_RESET));
  }

  @Test
  void isValidEmail() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);
    assertTrue(accountManagementTraitement.isValidEmail("plo@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo56@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo56.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo56.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo_56.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo.56.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("56plo.tri@plotri.fr"));
    assertTrue(accountManagementTraitement.isValidEmail("plo-56.tri@plotri.fr"));
    assertFalse(accountManagementTraitement.isValidEmail("@56plo.tri@plotri.fr"));
  }

  @Test
  void isTokenValid() {
    AccountManagementTraitement accountManagementTraitement = new AccountManagementTraitement(appUserRepository, appUserTockenRepository, mailTraitement);
    long DAY_IN_MS = 1000 * 60 * 60 * 24;
    long MS_BEFORE = 500;
    Date dateCreation = new Date(System.currentTimeMillis() - MS_BEFORE);
    AppUserToken appUserToken = new AppUserToken(null, "test.token@test.fake", "fake.token", dateCreation, null);
    assertTrue(accountManagementTraitement.isTokenValid(appUserToken).getErrCode() == 0);

    Date oldDate = new Date(System.currentTimeMillis() - (2 * DAY_IN_MS));
    appUserToken.setDateCreation(oldDate);
    assertTrue(accountManagementTraitement.isTokenValid(appUserToken).getErrCode() == ErrorMessages.ERR331);
    appUserToken.setDateCreation(new Date());
    appUserToken.setDateModification(new Date());
    assertTrue(accountManagementTraitement.isTokenValid(appUserToken).getErrCode() == ErrorMessages.ERR331);


  }

  @Test
  void testIsTokenValid() {
  }

  @Test
  void isTokenExists() {
  }
}
