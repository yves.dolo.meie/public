package ydo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.text.ParseException;

@SpringBootApplication
@EnableMongoRepositories(basePackages = {"ydo"}) // Sinon @Autowired Repo ne fonctionnent plus
@ComponentScan(basePackages = { "ydo"}) // Sinon @Autowired ne fonctionnent pas
public class Main {

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args)  throws ParseException {
		SpringApplication.run(Main.class, args);

    log.info("ok");

	}

}
