package ydo.lapaslaWApp.securite.security.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "refreshtoken")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RefreshToken {
  /*
   * Remplacement de l'Object Id par String
   */
  // ObjectId id
  String id;
  String token;
  Date dateCreation;
  Date dateRevocation;

}
