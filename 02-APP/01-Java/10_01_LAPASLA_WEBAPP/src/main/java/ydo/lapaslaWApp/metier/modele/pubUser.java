package ydo.lapaslaWApp.metier.modele;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class pubUser {
  String id;
  String firstName;
  String lastName;
  String pseudo;
}
