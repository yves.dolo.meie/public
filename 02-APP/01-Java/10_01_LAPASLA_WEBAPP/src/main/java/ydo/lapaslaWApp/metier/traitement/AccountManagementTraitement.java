package ydo.lapaslaWApp.metier.traitement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ydo.lapaslaWApp.applicatif.AppReponse;
import ydo.lapaslaWApp.applicatif.AppReponseAppUser;
import ydo.lapaslaWApp.applicatif.AppReponseAppUserToken;
import ydo.lapaslaWApp.config.Messages;
import ydo.lapaslaWApp.config.UseCase;
import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;
import ydo.lapaslaWApp.metier.mapper.AppUser2USERMapper;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.traitement.mail.MailTraitement;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import ydo.lapaslaWApp.securite.security.repository.AppUserTockenRepository;
import ydo.lapaslaWApp.securite.security.service.Utils.Commun;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ydo.lapaslaWApp.metier.controller.response.ErrorMessages.*;

/**
 * Classe permettant de réaliser tous les contrôles vis à vis de l'existance d'un compte
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Service
public class AccountManagementTraitement {

  private static final Class clazz = AccountManagementTraitement.class;
  private static final Logger log = LoggerFactory.getLogger(AccountManagementTraitement.class);

  @Autowired
  private AppUserRepository appUserRepository;
  @Autowired
  private AppUserTockenRepository appUserTockenRepository;
  @Autowired
  private MailTraitement mailTraitement;

  /**
   * Vérifie si un user existe en base de données
   * @param  mail : adresse mail de l'utilisateur
   * @return 0 s'il existe, un code erreur correspondant à la raison de sa non existence
   */
  public AppReponseAppUser isUserExists(String mail) {
    AppReponseAppUser appReponseAppUser = new  AppReponseAppUser();

    AppUser appUser = null;

    if (!isValidEmail(mail)) { appReponseAppUser = new  AppReponseAppUser(ERR220, 2, clazz); }
    if (appReponseAppUser.getErrCode() == 0 ) {
      appUser = appUserRepository.findByUsername(mail);
      appReponseAppUser = new AppReponseAppUser(appUser);
    }
    if (appUser == null ) { appReponseAppUser = new  AppReponseAppUser(ERR100, 2, clazz); }
    return  appReponseAppUser;
  }

  /**
   * Vérifie si un user existe en base de données
   * @param user
   * @return
   */
  public AppReponseAppUser isUserExists(User user) {
    return  isUserExists(user.getLogin()) ;
  }

  /**
   * Sauve en base de données  un user
   * @param user
   * @return
   */
  public AppReponseAppUser saveUser(User user) {
    AppReponseAppUser appReponseAppUser = new AppReponseAppUser(ERR10, 2, clazz);

    // Si le pseudo est vide ou contient des espaces, on remplace la chaine par le nom et prénom
    if (user.getPseudo() == null || user.getPseudo().replaceAll("\\s+", "").length() == 0) {
      user.setPseudo(user.getLastName() + " " + user.getFirstName());
    }

    AppUser appUser = AppUser2USERMapper.MAPPER.sourceToTarget(user);

    appUser.setAuthorities("ROLE_UTILISATEUR");
    // Chiffre le mot de passe
    appUser.setPassword(Commun.BCryptPwd(user.getPassword()));
    appUser.setDateCreation(new Date());
    appUser.setDateModification(new Date());
    AppUser savedAppUser = appUserRepository.save(appUser);
    if (savedAppUser != null) { appReponseAppUser = new AppReponseAppUser(savedAppUser); };
    return  appReponseAppUser;
  }


  /**
   * Changement de mot de passe d'un utilisateur
   * @param appUser
   * @param newPassword
   * @return
   */
  public AppReponseAppUser changePassword(AppUser appUser, String newPassword) {
    AppReponseAppUser appReponseAppUser = new AppReponseAppUser(ERR333, 2, clazz);
    // Chiffre le mot de passe
    appUser.setPassword(Commun.BCryptPwd(newPassword));
    appUser.setDateModification(new Date());
    AppUser savedAppUser = appUserRepository.save(appUser);
    if ( savedAppUser != null) { appReponseAppUser = new AppReponseAppUser(savedAppUser);}
    return appReponseAppUser;
  }

  /**
   * Positionne une date de modification à tous les tokens non null liés à l'utilsateur
   * @param appUser
   * @return
   */
  public AppReponseAppUserToken revokeAllTokens(AppUser appUser) {
    AppReponseAppUserToken appReponseAppUserToken = new AppReponseAppUserToken();
    //List<AppUserToken> appUserTokens = appUserTockenRepository.updateManyOpenTokens(appUser.getUsername());
    return appReponseAppUserToken;
  }

  /**
   * Notifie par mail un user
   * @param appUser : Caractéristiques de l'utilisateur
   * @return
   */
  public Integer notifMailUser(AppUser appUser, String useCase) {
    int errCode = 0;
    String userName = appUser.getUsername();

    switch (useCase) {
      case UseCase.REGISTER :
        String tokenR = getTocken();
        errCode = saveToken(userName, tokenR);
        if (errCode == 0 ) {
          String message = Messages.mailMessageUserCreatedHTML
            .replace(Messages.mailNomToChange, appUser.getFirstName())
            .replace(Messages.mailTokenToChange, tokenR);
          mailTraitement.sendMailToUser(userName, Messages.mailObjectUserCreated, message); // errCode > 0 ne sert à rien parce que l'erreur est non bloquante
        }
        break;
      case UseCase.FORGOT_PWD :
        String tokenF = getTocken();
        errCode = saveToken(userName, tokenF);
        if (errCode == 0 ) {
          String message = Messages.mailMessageForgotPasswordHTML
            .replace(Messages.mailNomToChange, appUser.getFirstName())
            .replace(Messages.mailTokenToChange, tokenF);
          errCode = mailTraitement.sendMailToUser(userName, Messages.mailObjectForgotPassword, message);
        }
        break;
      case UseCase.PWD_RESET :
        String message = Messages.mailMessageResetPasswordHTML
          .replace(Messages.mailNomToChange, appUser.getFirstName());
        errCode = mailTraitement.sendMailToUser(userName, Messages.mailObjectResetedPassword, message);
        break;
    }

    return  errCode;
  }


  public boolean isValidEmail(String email)
  {
    //String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
    String regex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    if (email != null)
    {
      Pattern p = Pattern.compile(regex);
      Matcher m = p.matcher(email);
      return m.find();
    }
    return false;
  }


  /**
   * Vérifie si un token est valide
   * @param  appUserToken : token dont les règles de validité doivent être vérifiées
   * @return 0 s'il est valide, un code erreur correspondant à la raison de sa non validité
   */
  public AppReponseAppUserToken isTokenValid(AppUserToken appUserToken) {
    AppReponseAppUserToken appReponseAppUserToken = new AppReponseAppUserToken(appUserToken);
    //diff in msec
    long diff = new Date().getTime() - appUserToken.getDateCreation().getTime();
    //diff in days
    long days = diff / (24 * 60 * 60 * 1000);
    // Si le tocken a déjà été consommé ou bien il a été généré il y a plus de 24h
    if (appUserToken.getDateModification() != null || days > 1 ) { appReponseAppUserToken = new AppReponseAppUserToken(ERR331, 2, clazz);     }
    return  appReponseAppUserToken;
  }


  /**
   * Vérifie si un token existe.
   * L'absence de token est bloquante pour la réinitialisation d'un mot de passe, non bloquante pour la création d'un compte
   * @param  token : token pour confirmer la création d'un compte, la réinitialisation d'un mot de passe
   * @return 0 s'il existe, un code erreur correspondant à la raison de sa non existence
   */
  public AppReponseAppUserToken isTokenExists(String token) {
    AppReponseAppUserToken appReponseAppUserToken = new  AppReponseAppUserToken();

    AppUserToken appUserToken = appUserTockenRepository.findByTocken(token);
    if (appUserToken == null) { appReponseAppUserToken = new AppReponseAppUserToken(ERR112, 1, clazz); }
    else {appReponseAppUserToken = new AppReponseAppUserToken(appUserToken, 0 );}
    return  appReponseAppUserToken;
  }

  /**
   * Enregistre un token en base de données
   * @param userName
   * @param token
   * @return
   */
  private int saveToken(String userName, String token) {
    int errCode = 0;
    AppUserToken appUserToken = new AppUserToken();
    appUserToken.setUsername(userName);
    appUserToken.setDateCreation(new Date());
    appUserToken.setTocken(token);
    if (appUserTockenRepository.save(appUserToken) == null) {
      errCode = ErrorMessages.ERR110; // La sauvegarde a échoué
      log.info("Erreur {} : {}", errCode, ErrorMessages.errorMessages.get(errCode));
    }
    return errCode;

  }

  private int invalidateToken(String token) {
    int errCode = 0;
    AppUserToken appUserToken = new AppUserToken();
    appUserToken.setDateModification(new Date());
    appUserToken.setTocken(token);
    if (appUserTockenRepository.findByTocken(token) == null) {
      errCode = ErrorMessages.ERR110; // La sauvegarde a échoué
      log.info("Erreur {} : {}", errCode, ErrorMessages.errorMessages.get(errCode));
    }
    return errCode;

  }



  private String getTocken() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }

}
