package ydo.lapaslaWApp.metier.traitement.mail;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ydo.lapaslaWApp.Config;
import ydo.lapaslaWApp.config.AdditionalProperties;
import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;

import java.io.IOException;

@Data
@NoArgsConstructor
@Service
public class MailTraitement {
  static final String SENDGRID_API_KEY = "SG.aBcDeFgHiJkLmNoPqRstuV.1aB2cD3eF4gH5iJ6kL7mN8oP9-qR0-sT1uV2wX3yZ4a";
  static final String SENDGRID_SENDER = "no-reply@saucrate.fr";
  static final String TO_EMAIL_CCI = "yves.dolo.meie@gmail.com";

  @Autowired
  private AdditionalProperties additionalProperties;

  private static final Logger log = LoggerFactory.getLogger(MailTraitement.class);

  public Integer sendMailToUser(String userMail, String subject, String body)  {
    int errCode = 0;
    Email to = new Email(this.additionalProperties.getEnv() != null && this.additionalProperties.getEnv().equals("dev") ? TO_EMAIL_CCI : userMail);
    Email from = new Email(SENDGRID_SENDER);
    Content content = new Content("text/html", body);
    Mail mail = new Mail(from, subject, to, content);
    /*
    Personalization personalization = new Personalization();
    personalization.addBcc(new Email(TO_EMAIL_CCI));
    mail.addPersonalization(personalization);
     */
    try {
      errCode = sendMail(mail);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return errCode;
  }

  private int sendMail(Mail mail) throws IOException {
    int errCode = 0;
    // Instantiates SendGrid client.
    SendGrid sendgrid = new SendGrid(SENDGRID_API_KEY);

    // Instantiate SendGrid request.
    Request request = new Request();

    // Set request configuration.
    request.setMethod(Method.POST);
    request.setEndpoint("mail/send");
    request.setBody(mail.build());

    // Use the client to send the API request.
    Response response = sendgrid.api(request);

    if (response.getStatusCode() != 202) {
      log.info("An error occurred: {}", response.getStatusCode());
      errCode = ErrorMessages.ERR510000;
    }
    return errCode;
  }


  }
