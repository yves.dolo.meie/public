package ydo.lapaslaWApp.metier.controller;

import ydo.lapaslaWApp.metier.modele.La;
import ydo.lapaslaWApp.metier.service.LaService;
import ydo.lapaslaWApp.metier.service.commun.LaPasLaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class LaController extends LaPasLaController {
  private static final Logger log = LoggerFactory.getLogger(LaController.class);

  @Autowired
  private LaService laService;

  /**
   * Récupère le détail de l'objet La
   * @param id : identifiant de l'objet recherché
   * @return détail de l'objet enregistré
   */
  @PreAuthorize("hasAnyRole('ROLE_UTILISATEUR')")
  @RequestMapping(method=RequestMethod.GET, value="/la/{id}")
  public La getLaQui(@PathVariable("id") String id) {
    return laService.getLa(id);

  }

  @PreAuthorize("hasAnyRole('ROLE_UTILISATEUR')")
  @RequestMapping(method=RequestMethod.GET, value="/allla")
  public List<La> getAllLa() {
    return laService.getLa();
  }


  @PreAuthorize("hasAnyRole('ROLE_UTILISATEUR')")
  @RequestMapping(method=RequestMethod.GET, value="/la")
  public List<La> getLaFromDate(
    @RequestParam(value="d", required=false ) String date
  ) {
    log.info("Date reçue : {}", date);
    return laService.getLaFromDate(date, this.tockenExtracteurService.getTocken().getName());
  }




  /**
   * Enregistre l'objet La
   * @param la : détail de l'objet à enregistrer
   * @return détail de l'objet enregistré
   */
  @PreAuthorize("hasAnyRole('ROLE_UTILISATEUR')")
  @RequestMapping(method=RequestMethod.POST, value="/la")
  public La getLaQui(@RequestBody La la) {
    return laService.postLa(la, this.tockenExtracteurService.getTocken().getName());

  }


}
