package ydo.lapaslaWApp.metier.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckProbe {

    private static final Logger log = LoggerFactory.getLogger(HealthCheckProbe.class);

    @RequestMapping(method= RequestMethod.GET, value="/")
    public ResponseEntity getHTTP200() {
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}
