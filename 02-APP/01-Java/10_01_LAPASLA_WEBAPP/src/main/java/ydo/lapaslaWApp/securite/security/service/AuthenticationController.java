package ydo.lapaslaWApp.securite.security.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import ydo.lapaslaWApp.metier.controller.response.ErrorResponse;
import ydo.lapaslaWApp.metier.service.RefreshTokenService;
import ydo.lapaslaWApp.securite.security.JwtUtils;
import ydo.lapaslaWApp.securite.security.Secret;
import ydo.lapaslaWApp.securite.security.model.*;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;

import static ydo.lapaslaWApp.securite.security.JwtUtils.generateHMACToken;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("api")
public class AuthenticationController {

  private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  AppUserRepository appUserRepository;

  @Autowired
  private RefreshTokenService refreshTokenService;

  int expirationInMinutes = 2*60;

  @RequestMapping(method = POST, value="/auth")
  public ResponseEntity<?> authenticationRequest(@RequestBody FullAuthenticationRequest fullAuthenticationRequest)
    throws AuthenticationException, IOException, JOSEException {

    String username = fullAuthenticationRequest.getUsername();
    String password = fullAuthenticationRequest.getPassword();
    // DEB YDO 2020 08 13
    String audience = fullAuthenticationRequest.getAudience();
    System.out.println("\n\nAudience = " + audience);
    // FIN YDO 2020 08 13

    // throws authenticationException if it fails !
    Authentication authentication = this.authenticationManager.authenticate(
      new UsernamePasswordAuthenticationToken(username, password)
    );
    SecurityContextHolder.getContext().setAuthentication(authentication);



    /** DEB YDO 2018 09 14
     * Remplacement de l'appel systématique de la ressource fichier secret.key par l'appel de la donnée en mémoire
     * Remplacement de username par l'id technique correspondant
     String secret = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("secret.key"), Charset.defaultCharset());
     String token = generateHMACToken(username, authentication.getAuthorities(), secret, expirationInMinutes);
     */
    String secret = Secret.getSecret();
    AppUser appUser = appUserRepository.findByUsername(username);
    String id = appUser.getId();
// DEB YDO 2020 08 13
//    String token = generateHMACToken(id, authentication.getAuthorities(), secret, expirationInMinutes);
    String token = JwtUtils.generateHMACToken(id, authentication.getAuthorities(), audience, secret, expirationInMinutes);
// FIN YDO 2020 08 13

    /** FIN YDO 2018 09 14
     *
     */

    // Return the token
    return ResponseEntity.ok(new FullAuthenticationResponse(appUser.getPseudo(), appUser.getFirstName(), appUser.getLastName(), token, refreshTokenService.create().getToken()));
  }


  /**
   * Reauthentification : fonctionne seulement si le tocken a expiré depuis moins de 7 jours. Récupère les informations du précédent tocken
   * Ne sert à rien de faire une fonction plus sécurisée où l'on trappe l'exprimation du tocken une deuième fois liée à la réauthent ?
   * @return
   * @throws AuthenticationException
   * @throws IOException
   * @throws JOSEException
   */
  @RequestMapping(method= RequestMethod.POST, value="/reauth")
  public ResponseEntity<?> reAuthenticationRequest(@RequestBody AuthenticationRequest authenticationRequest)
    throws AuthenticationException, IOException, JOSEException, ParseException {
    log.info("Renouvellement du Tocken {}", authenticationRequest.getToken());

    int errCode = 0;
    String newTocken = null;
    String secret = Secret.getSecret();
    SignedJWT signedJWT = SignedJWT.parse(authenticationRequest.getToken());

    // Vérification de la conformite du tocken
    if(!JwtUtils.verifyHMACToken(signedJWT, secret)) {
      errCode = 42;
    } else {
      log.info("\n Id de l'utilisateur : {} \n Audience : {}", JwtUtils.getUsername(signedJWT), JwtUtils.getAudience(signedJWT));
      AppUser appUser = appUserRepository.findById(JwtUtils.getUsername(signedJWT)).get();
      if (appUser != null) {
        // Vérification si le mot de passe a été changé entre temps ==> la réauthentification échoue et débouche sur la page de login
        if ( appUser.getDateModification().after(JwtUtils.getIssueTime(signedJWT))) {
          errCode = 43;
        } else {
          newTocken = JwtUtils.generateHMACToken(JwtUtils.getUsername(signedJWT), JwtUtils.getRoles(signedJWT), JwtUtils.getAudience(signedJWT), secret, expirationInMinutes);
          // Vérification de la durée de vie du refresh Tocken


          String todo1 = "Contrôler la validaité du token en base";
          String todo2 = "Mettre à jour la revocation du token y compris au logout, et changement de pwd";
          log.warn("\nTO DO : \n{} \n{}", todo1, todo2);
        }
      } else {
        errCode = 71;
      }
    }

    // Réponse
    switch (errCode) {
      case 0:
        return ResponseEntity.ok(new AuthenticationResponse(newTocken, refreshTokenService.create().getToken()));
      case 40 :
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorResponse(errCode));
      default:
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(errCode));
    }

  }

}
