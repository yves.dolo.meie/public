package ydo.lapaslaWApp.securite.security;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public final class Secret {

    public static String getSecret() throws IOException {

        Resource resource = new ClassPathResource("secret.key");
        InputStream resourceInputStream = resource.getInputStream();

        String secret;
        secret=IOUtils.toString(resourceInputStream, Charset.defaultCharset());

        return secret;
    }
}
