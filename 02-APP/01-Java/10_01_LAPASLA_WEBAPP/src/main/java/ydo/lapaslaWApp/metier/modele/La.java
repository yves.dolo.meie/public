package ydo.lapaslaWApp.metier.modele;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class La {
  public String qui;
  public String id;
  public Date quandJ;
  public String quandH; // au format HH:mm;
  public String quoi;
  public Boolean fige;
  public int la; // 1 = là, 2 = peut-être, 3 = Pas là, 0 = 'ne se prononce pas'
  public Date creeLe;
  public String creePar;
  public String key;
}
