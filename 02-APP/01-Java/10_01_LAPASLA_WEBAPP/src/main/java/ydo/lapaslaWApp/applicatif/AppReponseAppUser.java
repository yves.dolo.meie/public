package ydo.lapaslaWApp.applicatif;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import ydo.lapaslaWApp.securite.security.model.AppUser;

@Data
public class AppReponseAppUser extends AppReponse<AppUser> {
  public AppReponseAppUser(AppUser appUser) {
    super(appUser);
  }

  public AppReponseAppUser(Integer errCode, Integer errLevel, Class<?> clazz) {
    super(errCode, errLevel, clazz);
  }

  public AppReponseAppUser(AppUser objetReponse, Integer errCode) {
    super(objetReponse, errCode);
  }

  public AppReponseAppUser() {
    super();
  }
}
