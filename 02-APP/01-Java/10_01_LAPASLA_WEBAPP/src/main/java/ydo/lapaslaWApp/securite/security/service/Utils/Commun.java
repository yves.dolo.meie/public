package ydo.lapaslaWApp.securite.security.service.Utils;


import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Commun {


  /**
   * Encodage du password en utilisant le format BCrypt
   * @param pwd : mot de passe à chiffrer
   * @return mot de passe chiffré à stocker
   */
  public static String BCryptPwd(String pwd) {
    PasswordEncoder passwordEncoder =
      PasswordEncoderFactories.createDelegatingPasswordEncoder();
    /*
    String encodedPwd = passwordEncoder.encode(pwd);
    System.out.println(" Matching : " + passwordEncoder.matches(pwd, encodedPwd));*/

    return passwordEncoder
      .encode(pwd)
      .replace("{bcrypt}","");
  }

}
