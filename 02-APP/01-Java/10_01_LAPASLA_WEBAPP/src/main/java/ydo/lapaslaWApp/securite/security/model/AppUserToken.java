package ydo.lapaslaWApp.securite.security.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "appuserstocken")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppUserToken {
    // ObjectId id
    String id;
    String username; // email existant et connu
    String tocken;
    Date dateCreation;
    Date dateModification; // correspond à la consommation du tocken

}
