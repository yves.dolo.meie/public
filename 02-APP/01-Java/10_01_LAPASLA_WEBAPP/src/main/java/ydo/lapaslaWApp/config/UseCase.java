package ydo.lapaslaWApp.config;

public class UseCase {
  public static final String REGISTER = "REGISTER";
  public static final String FORGOT_PWD = "FORGOT_PWD";
  public static final String PWD_RESET = "PWD_RESET";
  public static final String DELETE_ACCOUNT = "DELETE_ACCOUNT";
}
