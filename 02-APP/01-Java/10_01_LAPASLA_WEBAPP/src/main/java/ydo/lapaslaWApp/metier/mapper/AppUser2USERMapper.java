package ydo.lapaslaWApp.metier.mapper;

import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AppUser2USERMapper {

  AppUser2USERMapper MAPPER = Mappers.getMapper(AppUser2USERMapper.class);

  @Mapping(source = "login", target = "username")
  // @Mapping(source = "email", target = "login")
  AppUser sourceToTarget(User user);

  @InheritInverseConfiguration
  User targetToSource(AppUser appUser);

}
