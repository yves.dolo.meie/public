package ydo.lapaslaWApp.securite.security;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.util.DateUtils;
import ydo.lapaslaWApp.securite.security.exceptions.JwtBadSignatureException;
import ydo.lapaslaWApp.securite.security.exceptions.JwtExpirationException;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.nimbusds.jose.JWSAlgorithm.HS256;

public final class JwtUtils {

  private final static String AUDIENCE_UNKNOWN = "unknown";
  private final static String AUDIENCE_WEB = "web";
  private final static String AUDIENCE_MOBILE = "mobile";
  private final static String AUDIENCE_TABLET = "tablet";
  private final static String ROLES_CLAIM = "roles";

  // DEB YDO 2020 08 13
/*
  public static String generateHMACToken(String subject, Collection<? extends GrantedAuthority> roles, String secret, int expirationInMinutes) throws JOSEException {
    return generateHMACToken(subject, AuthorityListToCommaSeparatedString(roles), secret, expirationInMinutes);
  }
*/
  public static String generateHMACToken(String subject, Collection<? extends GrantedAuthority> roles, String audience, String secret, int expirationInMinutes) throws JOSEException {
    return generateHMACToken(subject, AuthorityListToCommaSeparatedString(roles), audience, secret, expirationInMinutes);
  }

  // FIN YDO 2020 08 13

  // DEB YDO 2020 08 13
  // public static String generateHMACToken(String subject, String roles, String secret, int expirationInMinutes) throws JOSEException {
  public static String generateHMACToken(String subject, String roles, String audience, String secret, int expirationInMinutes) throws JOSEException {
    // FIN YDO 2020 08 13
    JWSSigner signer = new MACSigner(secret);
    JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
      .subject(subject)
      .issueTime(currentDate())
      .expirationTime(expirationDate(expirationInMinutes))
      .claim(ROLES_CLAIM, roles)
      .audience(audience != null && audience.equals(AUDIENCE_MOBILE) ? AUDIENCE_MOBILE : AUDIENCE_WEB)
      .build();

    SignedJWT signedJWT = new SignedJWT(new JWSHeader(HS256), claimsSet);
    signedJWT.sign(signer);
    return signedJWT.serialize();
  }

  private static Date currentDate() {
    return new Date(System.currentTimeMillis());
  }

  private static Date expirationDate(int expirationInMinutes) {
    return new Date(System.currentTimeMillis() + expirationInMinutes*60*1000);
  }

  public static void assertNotExpired(SignedJWT jwt) throws ParseException {
    if(DateUtils.isBefore(jwt.getJWTClaimsSet().getExpirationTime(), currentDate(), 60)) {

      // YDO 2020 07 23
      // throw new JwtExpirationException("Token has expired");
      throw new JwtExpirationException("41");
      // FIN YDO 2020 07 23
    }
  }

  public static void assertValidSignature(SignedJWT jwt, String secret) throws ParseException, JOSEException {
    if(!verifyHMACToken(jwt, secret)) {
      // YDO 2020 07 23
      // throw new JwtBadSignatureException("Signature is not valid");
      throw new JwtBadSignatureException("42");
      // FIN YDO 2020 07 23
    }
  }

  public static SignedJWT parse(String token) throws ParseException {
    return SignedJWT.parse(token);
  }

  public static boolean verifyHMACToken(SignedJWT jwt, String secret) throws ParseException, JOSEException {
    JWSVerifier verifier = new MACVerifier(secret);
    return jwt.verify(verifier);
  }

  private static String AuthorityListToCommaSeparatedString(Collection<? extends GrantedAuthority> authorities) {
    Set<String> authoritiesAsSetOfString = AuthorityUtils.authorityListToSet(authorities);
    return StringUtils.join(authoritiesAsSetOfString, ", ");
  }

  public static String getUsername(SignedJWT jwt) throws ParseException {
    return jwt.getJWTClaimsSet().getSubject();
  }

  // DEB YDO 2020 08 13
  public static String getAudience(SignedJWT jwt) throws ParseException {
    List<String> audiences = jwt.getJWTClaimsSet().getAudience();
    return audiences.size() > 0 ? audiences.get(0) : null;
  }
  // FIN YDO 2020 08 13

  public static Collection<? extends GrantedAuthority> getRoles(SignedJWT jwt) throws ParseException {
    Collection<? extends GrantedAuthority> authorities;
    String roles = jwt.getJWTClaimsSet().getStringClaim(ROLES_CLAIM);
    authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
    return authorities;
  }

  public static Date getIssueTime(SignedJWT jwt) throws ParseException {
    return jwt.getJWTClaimsSet().getIssueTime();
  }


  public static Date getExpirationTime(SignedJWT jwt) throws ParseException {
    return jwt.getJWTClaimsSet().getExpirationTime();
  }

}
