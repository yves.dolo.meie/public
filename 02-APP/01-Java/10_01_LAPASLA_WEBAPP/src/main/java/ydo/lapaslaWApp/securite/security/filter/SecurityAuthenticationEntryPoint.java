package ydo.lapaslaWApp.securite.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import ydo.lapaslaWApp.metier.controller.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.regex.Pattern;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * SecurityAuthenticationEntryPoint is called by ExceptionTranslationFilter to handle all AuthenticationException.
 * These exceptions are thrown when authentication failed : wrong login/password, authentication unavailable, invalid token
 * authentication expired, etc.
 *
 * For problems related to access (roles), see RestAccessDeniedHandler.
 */
public class SecurityAuthenticationEntryPoint implements AuthenticationEntryPoint {

  private static final Logger log = LoggerFactory.getLogger(SecurityAuthenticationEntryPoint.class);
  private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {


    ObjectMapper objMapper = new ObjectMapper();
    ErrorResponse errorResponse = null;

    HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
    wrapper.setStatus(SC_UNAUTHORIZED);
    wrapper.setContentType(APPLICATION_JSON_VALUE);
    // Parce que le message passé sous forme de String par Spring est parfois un numérique
    // dans sa forme customisée par moi
    String strNum = authException.getMessage();
    if (strNum == null || !pattern.matcher(strNum).matches()) {
      errorResponse = new ErrorResponse(strNum);
    } else {
      errorResponse = new ErrorResponse(Integer.valueOf(strNum));      }
    wrapper.getWriter().println(objMapper.writeValueAsString(errorResponse));
    wrapper.getWriter().flush();


  }
}
