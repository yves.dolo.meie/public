package ydo.lapaslaWApp.securite.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import ydo.lapaslaWApp.metier.controller.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.regex.Pattern;

import static org.apache.commons.httpclient.HttpStatus.SC_FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * The RestAccessDeniedHandler is called by the ExceptionTranslationFilter to handle all AccessDeniedExceptions.
 * These exceptions are thrown when the authentication is valid but access is not authorized.
 *
 */
public class RestAccessDeniedHandler implements AccessDeniedHandler {

  private static final Logger log = LoggerFactory.getLogger(RestAccessDeniedHandler.class);
  private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");


  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {

    ObjectMapper objMapper = new ObjectMapper();
    ErrorResponse errorResponse = null;

    HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
    wrapper.setStatus(SC_FORBIDDEN);
    wrapper.setContentType(APPLICATION_JSON_VALUE);
    // Parce que le message passé sous forme de String par Spring est parfois un numérique
    // dans sa forme customisée par moi
    String strNum = accessDeniedException.getMessage();
    if (strNum == null || !pattern.matcher(strNum).matches()) {
      errorResponse = new ErrorResponse(strNum);
    } else {
      errorResponse = new ErrorResponse(Integer.valueOf(strNum));
    }
    wrapper.getWriter().println(objMapper.writeValueAsString(errorResponse));
    wrapper.getWriter().flush();

  }
}
