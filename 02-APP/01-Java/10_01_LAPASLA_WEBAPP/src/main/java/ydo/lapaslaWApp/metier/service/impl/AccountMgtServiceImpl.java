package ydo.lapaslaWApp.metier.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ydo.lapaslaWApp.applicatif.AppReponse;
import ydo.lapaslaWApp.applicatif.AppReponseAppUser;
import ydo.lapaslaWApp.applicatif.AppReponseAppUserToken;
import ydo.lapaslaWApp.config.UseCase;
import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.service.AccountMgtService;
import ydo.lapaslaWApp.metier.traitement.AccountManagementTraitement;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;

import static ydo.lapaslaWApp.metier.controller.response.ErrorMessages.*;

@Service
public class AccountMgtServiceImpl implements AccountMgtService {
  private static final Class clazz = AccountMgtServiceImpl.class;
  private static final Logger log = LoggerFactory.getLogger(AccountMgtServiceImpl.class);

  @Autowired
  public AccountManagementTraitement accountManagementTraitement;

  public int postRegister( User user) {

    AppReponseAppUser appReponseAppUser = accountManagementTraitement.isUserExists(user);
    // Le user ne doit pas exister => isUserExists ==> ERR100
    if (appReponseAppUser.getErrCode() == ErrorMessages.ERR100) {
      appReponseAppUser = accountManagementTraitement.saveUser(user);
    } else {
      appReponseAppUser = new AppReponseAppUser(ERR20, 2, clazz);
    } // S'il existe alors on ne peut pas le créer
    if (appReponseAppUser.getErrCode() == 0) {
      accountManagementTraitement.notifMailUser(appReponseAppUser.getObjetReponse(), UseCase.REGISTER);
    } // envoi d'un mail, l'erreur qui en découle est non bloquante
    return appReponseAppUser.getErrCode();
  }

  /**
   * Vérifie l'existence du compte associé à l'adresse mail et génére un nouveau
   * tocken
   *
   * @param mail : doit être associée à un compte existant
   * @return
   */
  public int postNewPasswordTockenGenerator( String mail) {
    int errCode = 0;
    AppReponseAppUser appReponseAppUser = accountManagementTraitement.isUserExists(mail);
    // Le user doit exister => isUserExists ==> 0
    if (appReponseAppUser.getErrCode() == ErrorMessages.ERR100) {
      appReponseAppUser = new AppReponseAppUser(ERR31, 2, clazz);
      // ErrCode = 0 parce qu'il s'agit d'une erreur que peut exploiter un hacker
      errCode = 0;
    } else {
      errCode = accountManagementTraitement.notifMailUser(appReponseAppUser.getObjetReponse(), UseCase.FORGOT_PWD);
    }

    return errCode;
  }

  /**
   * Vérifie l'existence du token associé à la demande de réinitialisation ud mot
   * de passe
   *
   * @param tocken      : généré à partir d'une demande de réinitilisation
   * @param newPassword : nouveau password
   * @return
   */
  public int postResetPassword( String tocken,  String newPassword) {
     AppReponseAppUserToken appReponseAppUserToken = isTokenValid(tocken);
    AppReponseAppUser appReponseAppUser = new AppReponseAppUser(0, 0, clazz);
    // Si le token est valide, alors on recherche l'utilisateur associé
    if (appReponseAppUserToken.getErrCode() == 0) {
       AppUserToken appUserToken = appReponseAppUserToken.getObjetReponse();
      appReponseAppUser = isPwdChange(appUserToken.getUsername(), newPassword);
    } else {
      appReponseAppUser = new AppReponseAppUser(appReponseAppUserToken.getErrCode(),
          appReponseAppUserToken.getErrLevel(), clazz);
    }
    // Si la modification du mot de passe a bien été réalisée
    if (appReponseAppUser.getErrCode() == 0) {
       AppUser appUser = appReponseAppUser.getObjetReponse();
      // Si le changement de mot de passe a réussi, alors on notifie l'utilisateur de
      // la fin du traitement
      accountManagementTraitement.notifMailUser(appUser, UseCase.PWD_RESET);
      // Si le changement de mot passe a réussi, alors on revoke tous les tockens
      accountManagementTraitement.revokeAllTokens(appUser);
    }
    return appReponseAppUser.getErrCode();
  }

  /**
   * Confirme la création du compte client
   *
   * @param token : transmis par mail
   * @return si OK
   */
  public int postConfirmAccount( String token) {
     AppReponseAppUserToken appReponseAppUserToken = isTokenValid(token);
    // Si le token est valide, alors on recherche l'utilisateur associé
    if (appReponseAppUserToken.getErrCode() == 0) {
       AppUserToken appUserToken = appReponseAppUserToken.getObjetReponse();
    }
    return appReponseAppUserToken.getErrCode();
  }

  /**
   * Vérifie la validité du Token
   *
   * @return
   */
  private AppReponseAppUserToken isTokenValid( String tocken) {
    AppUserToken appUserToken = null;
    // Vérifie si le token existe
    AppReponseAppUserToken appReponseAppUserToken = accountManagementTraitement.isTokenExists(tocken);
    // L'absence de tocken est ici bloquante, et tous les autres contrôles échouent
    if (appReponseAppUserToken.getErrCode() == 0) {
      appUserToken = appReponseAppUserToken.getObjetReponse();
      appReponseAppUserToken = accountManagementTraitement.isTokenValid(appUserToken);
    }
    return appReponseAppUserToken;
  }

  private AppReponseAppUser isPwdChange( String userName,  String newPassword) {
    AppReponseAppUser appReponseAppUser = accountManagementTraitement.isUserExists(userName);
    // Si l'utilisateur associé est trouvé alors on change son password
    if (appReponseAppUser.getErrCode() == 0) {
       AppUser appUser = appReponseAppUser.getObjetReponse();
      appReponseAppUser = accountManagementTraitement.changePassword(appUser, newPassword);
    } else {
      appReponseAppUser = new AppReponseAppUser(ERR332, 0, clazz);
    }
    return appReponseAppUser;

  }

}
