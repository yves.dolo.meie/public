package ydo.lapaslaWApp.metier.service;


import ydo.lapaslaWApp.metier.modele.User;

public interface AccountMgtService {
  int postRegister(User user);
  int postNewPasswordTockenGenerator(String mail);
  int postResetPassword(String tocken, String password);
  int postConfirmAccount(String token);

}
