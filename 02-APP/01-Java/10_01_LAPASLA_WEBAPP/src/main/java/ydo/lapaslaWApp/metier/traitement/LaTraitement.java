package ydo.lapaslaWApp.metier.traitement;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Data
@NoArgsConstructor
@Component
public class LaTraitement {
    private static final Logger log = LoggerFactory.getLogger(LaTraitement.class);
    String userIdDemandeur;

    /**
     * Constructeurs
     */

    public LaTraitement(String userIdDemandeur) {
        this.userIdDemandeur = userIdDemandeur;
    }
}
