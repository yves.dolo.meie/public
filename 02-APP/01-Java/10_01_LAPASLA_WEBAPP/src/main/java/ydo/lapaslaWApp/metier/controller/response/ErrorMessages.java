package ydo.lapaslaWApp.metier.controller.response;

import java.util.HashMap;
import java.util.Map;

public class ErrorMessages {

  public static int ERR10 = 10;
  public static int ERR20 = 20;
  public static int ERR21 = 21;
  public static int ERR30 = 30;
  public static int ERR31 = 31;
  public static int ERR32 = 32;
  public static int ERR33 = 33;

  public static int ERR50 = 50;
  public static int ERR51 = 51;

  public static int ERR100 = 100;
  public static int ERR110 = 110;
  public static int ERR111 = 111;
  public static int ERR112 = 112;
  public static int ERR220 = 220;
  public static int ERR330 = 330;
  public static int ERR331 = 331;
  public static int ERR332 = 332;
  public static int ERR333 = 333;
  public static int ERR334 = 334;

  public static int ERR500 = 500;
  public static int ERR501 = 501;


  public static int ERR510000 = 510000;

  public static Map<Integer, String> errorMessages;
  public static Map<String, Integer> errorCodes;
  static {
    errorMessages = new HashMap<>();
    errorMessages.put(ERR10, "Le compte n'a pas pu être créé, et la cause est inconnue");

    errorMessages.put(ERR20, "Le compte n'a pas pu être créé, l'identifiant ou le mot de passe est invalide");
    errorMessages.put(ERR21, "Le compte n'a pas pu être créé, l'identifiant existe déjà");

    errorMessages.put(ERR30, "Si un compte correspond à l'adresse mail indiquée, un message de réinitialisation du mot de passe a été adressée à cette adresse.");
    errorMessages.put(ERR31, "La demande de réinitialisation du mot de passe n'a pas pu être réalisée, l'adresse mail est inconnue");
    errorMessages.put(ERR32, "La demande de réinitialisation du mot de passe a pu être réalisée mais la modification du tocken a échoué");


    errorMessages.put(40, "Authentification nécessaire, Le tocken n'est plus valide");
    errorMessages.put(41, "Authentification nécessaire, La durée du tocken a expiré");
    errorMessages.put(42, "Authentification nécessaire, la signature JWT est mauvaise");
    errorMessages.put(43, "Authentification nécessaire, le mot de passe a été changé depuis la demande de tocken");
    errorMessages.put(ERR50, "Autorisation nécessaire pour accéder à la ressource");
    errorMessages.put(60, "Login ou mot de passe inconnus");

    errorMessages.put(70, "L'accès au compte n'a pas pu être réalisé, veuillez contacter un administrateur'");
    errorMessages.put(71, "Le compte n'a pas été retrouvé, l'identifiant est inconnu");

    errorMessages.put(80, "L'envoi du mail a échoué");
    errorMessages.put(81, "L'envoi du mail de création de compte a échoué. S'il n'est pas réémis, le compte risque d'être résilié");

    errorMessages.put(ERR100, "Le compte client n'existe pas");

    errorMessages.put(ERR110, "La création du tocken a échoué");
    errorMessages.put(ERR111, "La modification du tocken a échoué");
    errorMessages.put(ERR112, "Le tocken est introuvable");


    errorMessages.put(ERR220, "L'identifiant ne répond pas à un format de mail valide");

    errorMessages.put(ERR330, "La demande de changement du mot de passe n'a pas pu être réalisée, le tocken n'existe pas");
    errorMessages.put(ERR331, "La demande de changement du mot de passe n'a pas pu être réalisée, le tocken a expiré");
    errorMessages.put(ERR332, "La demande de changement du mot de passe n'a pas pu être réalisée, le user associé au tocken n'a pas été retrouvé");
    errorMessages.put(ERR333, "La demande de changement du mot de passe n'a pas pu être réalisée, le sauvegarde du nouveau mot de passe a échoué");
    errorMessages.put(ERR334, "La demande de changement du mot de passe n'a pas été réalisée entièrement, la consommation du token a échoué");

    errorMessages.put(ERR501, "La demande de réinitialisation du mot de passe est temporairement indisponible. Réessayez ultérieurement");

    errorMessages.put(ERR510000, "L'envoi du mail a échoué");

  }

  static {
    errorCodes = new HashMap<>();
    errorCodes.put("Bad credentials", 60);
  }


}
