package ydo.lapaslaWApp.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ComponentScan("lapaslaWApp.securite")
// @ComponentScan("LAPASLAWApp.metier") // Appels Postgres fonctionnent sans
public class WebConfig {

}
