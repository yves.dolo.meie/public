package ydo.lapaslaWApp.metier.service.impl;

import ydo.lapaslaWApp.Config;
import ydo.lapaslaWApp.config.AdditionalProperties;
import ydo.lapaslaWApp.metier.mapper.LaMongoLaMapper;
import ydo.lapaslaWApp.metier.modele.La;
import ydo.lapaslaWApp.metier.modele.LaEntity;
import ydo.lapaslaWApp.metier.service.LaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;
import java.util.List;

@Service
public class LaServiceImpl implements LaService {
  private static final Logger log = LoggerFactory.getLogger(LaServiceImpl.class);

  @Autowired
  private AdditionalProperties additionalProperties;

  private static RestTemplate restTemplate = new RestTemplate();

  public La getLa(String id) {
    HttpHeaders headers4Get = new HttpHeaders();
    headers4Get.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entityGet = new HttpEntity<String>(headers4Get);
    ResponseEntity<La> laResponseEntity = restTemplate.exchange(this.additionalProperties.getNode_url(), HttpMethod.GET, entityGet, La.class, 100);
    System.out.println("Réponse http://localhost:9001/la : "+ laResponseEntity.getBody());

    La la = laResponseEntity.getBody();
    return la;
  }

  public List<La> getLa() {
    HttpHeaders headers4Get = new HttpHeaders();
    headers4Get.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entityGet = new HttpEntity<String>(headers4Get);
    ResponseEntity<List<LaEntity>> laResponseEntity = restTemplate.exchange(this.additionalProperties.getNode_url() + Config.NODE_PRESENCES_SAISIES_ENTRYPOINT, HttpMethod.GET, entityGet, new ParameterizedTypeReference<List<LaEntity>>(){}, 100);
    log.info("Réponse de {}{} : {} ", this.additionalProperties.getNode_url(), Config.NODE_PRESENCES_SAISIES_ENTRYPOINT, laResponseEntity.getBody());

    List<La> las = LaMongoLaMapper.MAPPER.targetToSource(laResponseEntity.getBody());

    return las;
  }


  public La postLa(La la, String createur) {
    la.setCreeLe(new Date());
    la.setCreePar(createur);
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<La> request = new HttpEntity<La>(la, headers);
    LaEntity laResponseEntity = restTemplate.postForObject(this.additionalProperties.getNode_url() + Config.NODE_LA_ENTRYPOINT, request, LaEntity.class);
    La createdLa  = LaMongoLaMapper.MAPPER.targetToSource(laResponseEntity);

    return createdLa;
  }

  public List<La> getLaFromDate(String date, String userId) {
    HttpHeaders headers4Get = new HttpHeaders();
    headers4Get.setContentType(MediaType.APPLICATION_JSON);

    // Query parameters
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(this.additionalProperties.getNode_url() + Config.NODE_PRESENCES_DATE_ENTRYPOINT)
      // Add query parameter
      .queryParam("d", date);
    System.out.println(builder.toUriString());
    System.out.println("Additionnal Properties : " + this.additionalProperties.getNode_url());


    HttpEntity<String> entityGet = new HttpEntity<String>(headers4Get);
    ResponseEntity<List<LaEntity>> laResponseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entityGet, new ParameterizedTypeReference<List<LaEntity>>(){}, 100);
    log.info("Réponse de {}{} : {} ", this.additionalProperties.getNode_url(), Config.NODE_PRESENCES_SAISIES_ENTRYPOINT, laResponseEntity.getBody());

    List<La> las = LaMongoLaMapper.MAPPER.targetToSource(laResponseEntity.getBody());

    // Pour tous les enregistrements qui ne correspondent pas au créateur / utilisateur, suppression de la valeur 'creePar'
    // Pour tous les enregistrements qui correspondent au créateur / utilisateur, remplacement de la valeur 'creePar' par qui
    // Ainsi il est possible de distinguer quels sont ses propres enregistrements, sans dévoiler son id.
    for( La la: las) {
      if (la.creePar.equals(userId)) {
        la.setCreePar(la.qui);
      } else {
        la.setCreePar(null);
      }
    }

    return las;
  }

}

