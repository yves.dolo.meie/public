package ydo.lapaslaWApp.securite.security.repository;


import ydo.lapaslaWApp.securite.security.model.RefreshToken;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends CrudRepository<RefreshToken, String> {
  RefreshToken findByToken(String token);
  Optional<RefreshToken> findById(String Id);
  RefreshToken save(RefreshToken refreshToken);
}


