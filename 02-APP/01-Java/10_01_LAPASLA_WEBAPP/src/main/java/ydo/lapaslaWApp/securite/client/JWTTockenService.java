package ydo.lapaslaWApp.securite.client;

import ydo.lapaslaWApp.securite.security.model.FullAuthenticationRequest;
import ydo.lapaslaWApp.securite.security.model.AuthenticationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

public class JWTTockenService {

	private static final Logger log = LoggerFactory.getLogger(JWTTockenService.class);
	private static final String uriStd = "http://localhost:cccc/";
	private static final String uri = "http://localhost:cccc/{entrypoint}";
	private static final RestTemplate restTemplate = new RestTemplate();
	public static String tocken;

	public static void getTocken() {

		String entrypoint = "auth";
		//AuthenticationRequest authParameters = new AuthenticationRequest("scolanet", "scolanet");
		FullAuthenticationRequest authParameters = new FullAuthenticationRequest("login", "pwd");

		// Set the headers...
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json"); // we are sending json
//		headers.set("Accept", "text/plain"); // looks like you want a string back


		HttpEntity<FullAuthenticationRequest> request = new HttpEntity<FullAuthenticationRequest>(authParameters);

		// URI (URL) parameters
		Map<String, String> uriParams = new HashMap<String, String>();
		uriParams.put("entrypoint", entrypoint);

		// Query parameters
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
				// Add query parameter
				//.queryParam("name", "Param ajoute par Query Param");

		System.out.println(builder.buildAndExpand(uriParams).toUri());
		log.info(request.toString());

		ResponseEntity<AuthenticationResponse> response = restTemplate.exchange(builder.buildAndExpand(uriParams).toUri() , HttpMethod.POST, request, AuthenticationResponse.class);
		log.info(" ***************  Retour : " + response.getBody());
		log.info(" ***************  Contenu de response : " + response.getBody().getToken());
		tocken = response.getBody().getToken();
	}


}
