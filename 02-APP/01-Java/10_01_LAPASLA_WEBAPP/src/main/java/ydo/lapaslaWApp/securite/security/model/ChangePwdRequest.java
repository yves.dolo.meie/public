package ydo.lapaslaWApp.securite.security.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangePwdRequest {
    private String oldPassword;
    private String newPassword;

}
