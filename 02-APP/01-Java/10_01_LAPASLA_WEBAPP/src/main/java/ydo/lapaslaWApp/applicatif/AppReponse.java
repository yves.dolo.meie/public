package ydo.lapaslaWApp.applicatif;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;

@Data
@NoArgsConstructor // Cas passant - tout va bien
/**
 * Objet de réponse contenant le code d'erreur et l'objet réponse si tout se passe bien
 * errLevel: 0 = Pas d'erreur, 1 = erreur non bloquante, 2 = erreur bloquante
 */
public abstract class AppReponse<T> {

  private static final Logger log = LoggerFactory.getLogger(AppReponse.class);

  Integer errCode = 0;
  Integer errLevel = 0;
  T objetReponse = null;


  /**
   * Reponse du traitement
   * @param errCode : code de l'erreur rencontrée
   * @param errLevel : niveau de l'erreur rencontrée
   * @param clazz : classe appelante
   */
  public AppReponse(Integer errCode, Integer errLevel, Class<?> clazz) {
    this.errCode = errCode;
    this.errLevel = errLevel;
    log.info("Erreur {}-{} : {} - {}", errCode, this.errLevel, ErrorMessages.errorMessages.get(errCode), clazz);
  }



  /**
   * Cas passant
   * @param objetReponse : objet obtenu en retour
   */
  public AppReponse( T objetReponse) {
    this(objetReponse, 0);
  }

  /**
   * Cas passant
   * @param errCode : en principe = 0
   * @param objetReponse : objet obtenu en retour
   */
  public AppReponse( T objetReponse, Integer errCode) {
    this.errCode = errCode;
    this.objetReponse = objetReponse;
  }




}
