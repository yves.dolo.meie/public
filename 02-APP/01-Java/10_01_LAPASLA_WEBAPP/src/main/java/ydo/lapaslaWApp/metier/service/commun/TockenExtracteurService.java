package ydo.lapaslaWApp.metier.service.commun;

import com.nimbusds.jwt.SignedJWT;
import ydo.lapaslaWApp.securite.security.JwtUtils;
import ydo.lapaslaWApp.securite.security.Secret;
import ydo.lapaslaWApp.securite.security.filter.JwtTokenAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ydo.lapaslaWApp.metier.modele.Tocken;

import javax.servlet.http.HttpServletRequest;

@Service
public class TockenExtracteurService {


    @Autowired
    HttpServletRequest request;
    protected Tocken tocken;

    public Tocken getTocken() {
        try {
            String secret = Secret.getSecret();
            JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter = new JwtTokenAuthenticationFilter("/**", secret);
            SignedJWT jwt = jwtTokenAuthenticationFilter.extractAndDecodeJwt(request);
            tocken = new Tocken(JwtUtils.getUsername(jwt), JwtUtils.getAudience(jwt), JwtUtils.getExpirationTime(jwt), JwtUtils.getRoles(jwt));
        } catch (Exception e) {
            // TODO: handle exception
        }
        return tocken;
    }
}
