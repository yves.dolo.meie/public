package ydo.lapaslaWApp.securite.security.service;

import com.nimbusds.jose.JOSEException;
import ydo.lapaslaWApp.securite.security.model.FullAuthenticationRequest;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/user/testChangePassword")
public class SimpleChangePwdController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    AppUserRepository appUserRepository;



    @RequestMapping(method = POST)
    public ResponseEntity<?> setPwdRequest(@RequestBody FullAuthenticationRequest fullAuthenticationRequest)
            throws AuthenticationException, IOException, JOSEException {

/**
 * YDO TO DO --> supprimer username car récupéré du tocken ?
 */
        String username = fullAuthenticationRequest.getUsername();
        String password = fullAuthenticationRequest.getPassword();


        PasswordEncoder passwordEncoder =
                PasswordEncoderFactories.createDelegatingPasswordEncoder();

        String encodedPwd =  passwordEncoder.encode(password);
        System.out.println(" Matching : " + passwordEncoder.matches(password, encodedPwd));


        // Return the token
        return ResponseEntity.ok("\n"
          + "Le résultat du nouveau password chiffré est :\n"
          + encodedPwd + "\n"
        + "Ce nouveau password n'est pas sauvegardé en base - Simplement pour des tests\n");
    }


}
