package ydo.lapaslaWApp.metier.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ydo.lapaslaWApp.metier.service.RefreshTokenService;
import ydo.lapaslaWApp.securite.security.model.RefreshToken;
import ydo.lapaslaWApp.securite.security.repository.RefreshTokenRepository;

import java.util.Date;
import java.util.UUID;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
  private static final Logger log = LoggerFactory.getLogger(RefreshTokenServiceImpl.class);


  @Autowired
  private RefreshTokenRepository refreshTokenRepository;


  public RefreshToken create() {
    RefreshToken refreshToken = new RefreshToken(null, UUID.randomUUID().toString(), new Date(), null );
    refreshToken = refreshTokenRepository.save(refreshToken);
    return refreshToken;
  }
  public RefreshToken revoke(){
    return null;
  }
  public RefreshToken verify(){
    return null;
  }

}
