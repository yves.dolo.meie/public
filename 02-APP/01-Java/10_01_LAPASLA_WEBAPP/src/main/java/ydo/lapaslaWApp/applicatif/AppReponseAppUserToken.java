package ydo.lapaslaWApp.applicatif;

import lombok.Data;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;

@Data
public class AppReponseAppUserToken extends AppReponse<AppUserToken> {
  public AppReponseAppUserToken(AppUserToken appUser) {
    super(appUser);
  }

  public AppReponseAppUserToken(Integer errCode, Integer errLevel, Class<?> clazz) {
    super(errCode, errLevel, clazz);
  }

  public AppReponseAppUserToken(AppUserToken objetReponse, Integer errCode) {
    super(objetReponse, errCode);
  }

  public AppReponseAppUserToken() {
    super();
  }
}
