package ydo.lapaslaWApp.securite.security.repository;


import ydo.lapaslaWApp.metier.modele.AllMongoQueries;
import ydo.lapaslaWApp.securite.security.model.AppUserToken;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface AppUserTockenRepository extends CrudRepository<AppUserToken, String> {
  AppUserToken findByUsername(String username);
  AppUserToken findByTocken(String tocken);
  Optional<AppUserToken> findById(String Id);
  AppUserToken save(AppUserToken appUserToken);

  @Query(AllMongoQueries.openedAppUserToken)
  List<AppUserToken> updateManyOpenTokens(String username);
}


