package ydo.lapaslaWApp.securite.security.model;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FullAuthenticationRequest {

    private String username;
    private String password;
    private String audience; // Pour gérer le renouvellement du tocken

  // DEB YDO 2020 08 13
  public FullAuthenticationRequest(String username, String password, String audience) {
    this.username = username;
    this.password = password;
    this.audience = audience;
  }
  // FIN YDO 2020 08 13

  public FullAuthenticationRequest(String username, String password) {
    this.username = username;
    this.password = password;
  }
}
