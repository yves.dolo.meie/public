package ydo.lapaslaWApp.config;

public class Messages {

  public static String mailObjectUserCreated = "Votre compte utilisateur a bien été créé";
  public static String mailObjectForgotPassword = "Demande de réinitialisation du mot de passe";
  public static String mailObjectResetedPassword = "Votre mot de passe a bien été modifié";
  public static String mailNomToChange = "##NOM##";
  public static String mailTokenToChange = "##TOKEN##";
  public static String mailMessageValiderCompte = "<a href=\"https://saucrate.fr/confirmAccount/" + mailTokenToChange + "\">Confirmer la création du compte.</a>";
  public static String mailMessageSupprimerCompte = "<a href=\"https://saucrate.fr/deleteAccount/" + mailTokenToChange + "\">Supprimer le compte.</a>";
  public static String mailMessageResetPwd = "<a href=\"https://saucrate.fr/resetpassword/" + mailTokenToChange + "\">Définir un nouveau mot de passe.</a>";
  public static  String mailSignature = "\nPloërmel Triathon";

  public static String mailMessageUserCreatedHTML =
    "<html>" +
      "<head></head>" +
      "<body>" +
      "    <p>" +
      "Chèr(e) " + mailNomToChange + ",<br><br>" +
      "Votre compte utilisateur a bien été créé.<br>" +
      "Afin de vérifier que vous êtes bien à l'origine de la création de ce compte, nous vous remercions de cliquer sur le lien suivant : "
      + mailMessageValiderCompte + ".<br>" +
      "En l'absence de validation, ce compte sera supprimé dans un délai de 48h.<br>" +
      "Si vous n'êtes pas à l'origine de ce mail et que vous souhaitez supprimer le compte cliquez sur " +
      mailMessageSupprimerCompte +
      "<br><br>" +
      "Sportivement,<br>" +
      mailSignature + "<br>" +
      "</p>" +
      "</body>" +
      "</html>";

  public static String mailMessageForgotPasswordHTML =
    "<html>" +
      "<head></head>" +
      "<body>" +
      "    <p>" +
      "Chèr(e) " + mailNomToChange + ",<br><br>" +
      "Afin de vous permettre de modifier votre mot de passe, nous vous remercions de cliquer sur le lien suivant : "
      + mailMessageResetPwd + ".<br>" +
      "En l'absence de validation, vous devrez procéder à une nouvelle demande de réinitilisation.<br>" +
      "Si vous n'êtes pas à l'origine de ce mail et que vous souhaitez supprimer le compte cliquez sur " +
      mailMessageSupprimerCompte +
      "<br><br>" +
      "Sportivement,<br>" +
      mailSignature + "<br>" +
      "</p>" +
      "</body>" +
      "</html>";


  public static String mailMessageResetPasswordHTML =
    "<html>" +
      "<head></head>" +
      "<body>" +
      "    <p>" +
      "Chèr(e) " + mailNomToChange + ",<br><br>" +
      "Votre nouveau mot de passe a bien été pris en compte.<br>" +
      "Si vous n'êtes pas à l'origine de ce mail et que vous souhaitez supprimer le compte cliquez sur " +
      mailMessageSupprimerCompte +
      "<br><br>" +
      "Sportivement,<br>" +
      mailSignature + "<br>" +
      "</p>" +
      "</body>" +
      "</html>";

}
