package ydo.lapaslaWApp.securite.security.repository;


import ydo.lapaslaWApp.securite.security.model.AppUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AppUserRepository extends CrudRepository<AppUser, String> {
  AppUser findByUsername(String username);
  Optional<AppUser> findById(String Id);
  AppUser save(AppUser appUser);
}


