package ydo.lapaslaWApp.securite.security.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "app_users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppUser {
    /** DEB YDO 14/09/18
     * Remplacement de l'Object Id par String
     */
    // ObjectId id
    String id;
    /* FIN YDO 14/09/2018 */
    String username;
    String password;
    String authorities;
    String firstName;
    String pseudo;
    String lastName;
    String email;
    String oldPassword; // Pour éviter de remettre le même pwd que le précédent
    Date dateCreation;
    Date dateModification;

}
