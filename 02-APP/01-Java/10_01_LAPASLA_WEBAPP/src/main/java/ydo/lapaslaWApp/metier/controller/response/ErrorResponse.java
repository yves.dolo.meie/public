package ydo.lapaslaWApp.metier.controller.response;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class ErrorResponse {
  private static final Logger log = LoggerFactory.getLogger(ErrorResponse.class);

  Integer code;
  String message;

  /**
   * Objet retourné en réponse en cas d'erreur
   * Seule l'erreur de la dizaine inférieure est retournée pour éviter de donner trop de détails sur la nature de l'erreur
   * et permettre à un hacker de profiter de l'information
   * @param code
   */
  public ErrorResponse(Integer code) {
    // Bien conserver log.info("{} {}", code ... car code n'est pas transformé
    log.info("{} {}", code, ErrorMessages.errorMessages.get(code));
    this.code = (code / 10) * 10;
    this.message = ErrorMessages.errorMessages.get(this.code);

  }

  /**
   * Objet retourné en réponse en cas d'erreur
   * Seule l'erreur de la dizaine inférieure est retournée pour éviter de donner trop de détails sur la nature de l'erreur
   * et permettre à un hacker de profiter de l'information
   * @param message à partir duquel le code peut être trouvé
   */
  public ErrorResponse(String message) {
    this.message = message;
    this.code = ErrorMessages.errorCodes.get(this.message);
    if (this.code == null) {
      this.code = -1;
    }
    log.info("{} {}", this.code, this.message);
  }


}
