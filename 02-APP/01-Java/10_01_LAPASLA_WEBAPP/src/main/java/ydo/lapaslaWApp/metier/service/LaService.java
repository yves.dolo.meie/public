package ydo.lapaslaWApp.metier.service;


import ydo.lapaslaWApp.metier.modele.La;

import java.util.List;

public interface LaService {
  La postLa(La la, String createur);
  La getLa(String id);
  List<La> getLa();
  List<La> getLaFromDate(String date, String userId);
  // String postSurvey(Survey survey);
}
