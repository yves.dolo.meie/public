package ydo.lapaslaWApp.metier.service.commun;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class LaPasLaController {
    @Autowired
    public TockenExtracteurService tockenExtracteurService;

    public String getUserId() {
        return tockenExtracteurService.getTocken().getName();
    }

    public Collection<? extends GrantedAuthority> getRoles() {
        return tockenExtracteurService.getTocken().getRoles();
    }

}
