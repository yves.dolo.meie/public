package ydo.lapaslaWApp.securite.security.service;

import com.nimbusds.jose.JOSEException;
import ydo.lapaslaWApp.metier.service.commun.TockenExtracteurService;
import ydo.lapaslaWApp.securite.security.model.AppUser;
import ydo.lapaslaWApp.securite.security.model.ChangePwdRequest;
import ydo.lapaslaWApp.securite.security.repository.AppUserRepository;
import ydo.lapaslaWApp.securite.security.service.Utils.Commun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("api/resetPassword")
public class ChangePwdController {

  private static final Logger log = LoggerFactory.getLogger(ChangePwdController.class);

  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  AppUserRepository appUserRepository;

  @Autowired
  private TockenExtracteurService tockenExtracteurService;



  @RequestMapping(method = POST)
  public ResponseEntity<?> setPwdRequest(@RequestBody ChangePwdRequest changePwdRequest)
    throws AuthenticationException, IOException, JOSEException {
/**
 * YDO TO DO --> supprimer username car récupéré du tocken ?
 */
    String oldPassword = changePwdRequest.getOldPassword();
    String newPassword = changePwdRequest.getNewPassword();


    // Récupération de l'identité de celui qui demande le changement de mot de passe
    String userId = tockenExtracteurService.getTocken().getName();
    System.out.println(String.format("tockenExtracteurService %s ", tockenExtracteurService.getTocken().getName()));
    AppUser appUser = appUserRepository.findById(userId).get();
    System.out.println(String.format("AppUser %s - Id %s - Pwd %s)", appUser.getUsername(), appUser.getId(), appUser.getPassword()));

    // Vérification que l'utilisateur pour lequel la demande de changement de mot passe existe bien
    // et dispose des bons crédits
    // throws authenticationException if it fails !
    Authentication authentication = this.authenticationManager.authenticate(
      new UsernamePasswordAuthenticationToken(appUser.getUsername(), oldPassword)
    );

    // Chiffre le mot de passe
    appUser.setPassword(Commun.BCryptPwd(newPassword));
    appUser = appUserRepository.save(appUser);
    System.out.println(String.format("AppUser %s - Id %s - Pwd %s)", appUser.getUsername(), appUser.getId(), appUser.getPassword()));


    // Return the token
    return ResponseEntity.ok("");
  }


}
