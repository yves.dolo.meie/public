package ydo.lapaslaWApp.metier.controller;

import ydo.lapaslaWApp.metier.controller.response.ErrorMessages;
import ydo.lapaslaWApp.metier.controller.response.ErrorResponse;
import ydo.lapaslaWApp.metier.modele.ResetPwdContext;
import ydo.lapaslaWApp.metier.modele.User;
import ydo.lapaslaWApp.metier.service.AccountMgtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class AccountMgtController {
  private static final Logger log = LoggerFactory.getLogger(AccountMgtController.class);

  @Autowired
  private AccountMgtService accountMgtService;

  /**
   * Enregistre un nouveau compte client
   * @param user : caractéristiques de l'utilisateur à enregistrer
   * @return détail de l'objet enregistré
   */
  @RequestMapping(method=RequestMethod.POST, value="/register")
  public ResponseEntity postRegister(@RequestBody User user) {
    int errCode = accountMgtService.postRegister(user);
    if (errCode == 0) {
      return ResponseEntity.ok().body(null);
    } else {
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ErrorResponse(errCode));
    }
  }

  /**
   * Envoie un mail à l'adresse demandée pour réinitialisation du mot de passe
   * @param mail : adresse mail à laquelle il faut envoyer le tocken
   * @return : Ok ou code Erreur
   */
  @RequestMapping(method=RequestMethod.POST, value="/newpassword")
  public ResponseEntity postNewPassword(@RequestBody String mail) {
    int errCode = accountMgtService.postNewPasswordTockenGenerator(mail);
    // Dans tous les cas on répond ok pour ne pas permettre à un Hacker de vérifier l'existence du mot de passe
    if (errCode == 0) {
      return ResponseEntity.ok().body(null);
    } else {
      // sauf en cas d'erreur technique
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ErrorResponse(ErrorMessages.ERR501));
    }
  }


  /**
   * Réinitialise le nouveau mot de passe avec celui qui est transmis
   * @param resetPwdContext : contexte contenant le tocken et le nouveau mot de passe
   * @return : Ok ou code Erreur
   */
  @RequestMapping(method=RequestMethod.POST, value="/resetpassword")
  public ResponseEntity postResetPassword(@RequestBody ResetPwdContext resetPwdContext) {
    int errCode = accountMgtService.postResetPassword(resetPwdContext.getTocken(), resetPwdContext.getPassword());
    if (errCode == 0) {
      return ResponseEntity.ok().body(null);
    } else {
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new ErrorResponse(errCode));
    }
  }

  /**
   * confirme la création du compte
   * @param token : contexte contenant le tocken et le nouveau mot de passe
   * @return : Ok ou code Erreur
   */
  @RequestMapping(method=RequestMethod.POST, value="/confirmaccount")
  public ResponseEntity postConfirmAccount(@RequestBody String token) {
    int errCode = accountMgtService.postConfirmAccount(token);
    if (errCode == 0) {
      return ResponseEntity.ok().body(null);
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(errCode));
    }
  }


}
