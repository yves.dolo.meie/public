package ydo.lapaslaWApp.metier.controller;

import ydo.lapaslaWApp.metier.service.commun.LaPasLaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
public class ErrorsMgVerifController extends LaPasLaController {
  private static final Logger log = LoggerFactory.getLogger(ErrorsMgVerifController.class);


  @PreAuthorize("hasAnyRole('ROLE_UTILISATEUR')")
  @RequestMapping(method=RequestMethod.GET, value="/test401")
  public String getErr401() {
    return "done";
  }


  @PreAuthorize("hasAnyRole('ROLE_xx')")
  @RequestMapping(method=RequestMethod.GET, value="/test403")
  public String getErr403() {
    return "done";
  }



}
