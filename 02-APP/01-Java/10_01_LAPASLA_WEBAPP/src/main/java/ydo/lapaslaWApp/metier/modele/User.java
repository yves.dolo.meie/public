package ydo.lapaslaWApp.metier.modele;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
  String id;
  String login;
  String password;
  String firstName;
  String lastName;
  String pseudo;
}
