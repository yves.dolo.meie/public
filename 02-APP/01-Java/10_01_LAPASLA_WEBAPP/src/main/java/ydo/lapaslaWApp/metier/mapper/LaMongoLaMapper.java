package ydo.lapaslaWApp.metier.mapper;

import ydo.lapaslaWApp.metier.modele.La;
import ydo.lapaslaWApp.metier.modele.LaEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LaMongoLaMapper {

  LaMongoLaMapper MAPPER = Mappers.getMapper(LaMongoLaMapper.class);

  @Mapping(source = "id", target = "_id")
  LaEntity sourceToTarget(La Classe);

  @InheritInverseConfiguration
  La targetToSource(LaEntity mongoClasse);



  List<LaEntity> sourceToTarget(List<La> Classes);

  @InheritInverseConfiguration
  List<La> targetToSource(List<LaEntity> mongoClasses);
}
