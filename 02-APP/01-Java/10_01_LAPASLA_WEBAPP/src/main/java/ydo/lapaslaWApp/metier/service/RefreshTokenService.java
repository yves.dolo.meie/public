package ydo.lapaslaWApp.metier.service;


import ydo.lapaslaWApp.securite.security.model.RefreshToken;

public interface RefreshTokenService {
  RefreshToken create();
  RefreshToken revoke();
  RefreshToken verify();
}
