package ydo.lapaslaWApp.metier.modele;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Tocken {
  String name; // clé du tocken
  String audience;
  Date expirationDate;
  Collection<? extends GrantedAuthority> roles; // roles
}
