#!/bin/sh
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH
echo "Configuration paramètres pour GCloud"
gcloud auth configure-docker
PROJECT_NAME='GCP_PROEJCT'  
PROJECT_ID='la-pas-la'  
CLUSTER_NAME='lapasla-cluster-1'
REGION_ID='europe-west1'  
ZONE='europe-west1-b'  
IMAGE_IHM='plotri/la-pas-la-ihm'
IMAGE_IHM_TAGGED=$IMAGE_IHM':v0'  
IMAGE_JAVA_APP='plotri/la-pas-la-app-java'
IMAGE_JAVA_APP_TAGGED=$IMAGE_JAVA_APP':v0' 
IMAGE_NODE_APP='plotri/la-pas-la-app-node'
IMAGE_NODE_APP_TAGGED=$IMAGE_NODE_APP':v0'    
gcloud config set project $PROJECT_ID  
gcloud config set run/region $REGION_ID  
gcloud config set compute/zone $ZONE  
gcloud config set compute/region $REGION_ID  
gcloud components update

# Build projet Angular pour la production
echo "Build projet Angular pour la production"
cd $SCRIPTPATH/01-IHM  
ng build ploTri --prod --base-href /

# Build de l'image Docker pour Angular
echo "Build de l'image Docker pour Angular"
docker build -f Dockerfile.http -t eu.gcr.io/$PROJECT_ID/$IMAGE_IHM_TAGGED --build-arg APPLICATION=ploTri .   

echo "Push de l'image Docker Angular sous GCloud Stockage"
docker push eu.gcr.io/$PROJECT_ID/$IMAGE_IHM_TAGGED  

# JAVA
cd $SCRIPTPATH/02-APP/01-Java/10_01_LAPASLA_WEBAPP
# Build du projet java
echo "Build de l'application pour mongoAtlas sous forme de .jar"
mvn clean install -Pgcp
docker build --build-arg PROFILE=gcp -t eu.gcr.io/$PROJECT_ID/$IMAGE_JAVA_APP_TAGGED .  
echo "Push de l'image Docker Java pour mongoAtlas sous GCloud Stockage"
docker push eu.gcr.io/$PROJECT_ID/$IMAGE_JAVA_APP_TAGGED

# Build du projet node
cd $SCRIPTPATH/02-APP/06-nodeApp
echo "Build de l'image Docker pour l'application Node"
docker build -t eu.gcr.io/$PROJECT_ID/$IMAGE_NODE_APP_TAGGED .  
echo "Push de l'image Docker pour l'application Node sous GCloud Stockage"
docker push eu.gcr.io/$PROJECT_ID/$IMAGE_NODE_APP_TAGGED

# Accès à l'environnement GCP
echo "Positionnement à l'environnement GCP Kubernetes"
gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE --project $PROJECT_ID
echo "Positionnement du cluster à 1 noeud"
echo "Y" | gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone=$ZONE


